﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Paint_2._0
{
    /// <summary>
    /// Класс, отвечает за загрузку и сохранение настроек приложения.
    /// Доступен в любом месте приложения. Хранится в XML файле
    /// </summary>
    static class SettingsManager
    {
        #region Члены
        // Постоянный объект приложения
        static Settings settings = new Settings();

        // Подкаталоги в Application Data, где хранятся настройки
        const string applicationDirectory = "Max&Sergey Paint2.0";

        // Имя файла настройки
        const string settingsFileName = "Settings.xml";

        #endregion Члены

        #region Конструктор
        static SettingsManager()
        {
            EnsureDirectoryExists();
        }

        #endregion Конструктор

        #region Свойства
        public static Settings ApplicationSettings
        {
            get { return settings; }
        }

        #endregion Свойства

        #region Запуск, выход
        /// <summary>
        /// Вызывается перед открытием приложения
        /// </summary>
        public static void OnStartup()
        {
            LoadSettings();
        }

        /// <summary>
        /// Вызывается перед закрытием приложения
        /// </summary>
        public static void OnExit()
        {
            SaveSettings();
        }

        #endregion Запуск, выход

        #region Другие функции
        /// <summary>
        /// Возвращает имя файла настроек
        /// </summary>
        static string SettingsFileName
        {
            get
            {
                return Path.Combine(
                    Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    applicationDirectory),
                    settingsFileName);
            }
        }

        /// <summary>
        /// Загружает настройки приложения из XML файла
        /// </summary>
        static void LoadSettings()
        {
            Settings tmp;
            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(Settings));
                using (Stream stream = new FileStream(SettingsFileName,
                    FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    tmp = (Settings)xml.Deserialize(stream);
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
                return;
            }
            // Заменяем начальные настройки, настройками из файла
            settings = tmp;
        }

        /// <summary>
        /// Сохраняем настройки в XML файл
        /// </summary>
        static void SaveSettings()
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(typeof(Settings));
                using (Stream stream = new FileStream(SettingsFileName,
                       FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    xml.Serialize(stream, settings);
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
        }

        static void EnsureDirectoryExists()
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    applicationDirectory));
                if (!info.Exists)
                {
                    info.Create();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }

        #endregion Другие функции
    }
}

