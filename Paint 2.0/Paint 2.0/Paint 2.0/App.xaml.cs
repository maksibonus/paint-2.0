﻿using System;
using System.Windows;
using System.Data;
using System.Xml;
using System.Configuration;

namespace Paint_2._0
{
    /// <summary>
    /// Класс приложения
    /// </summary>
    public partial class App : System.Windows.Application
    {
        #region Переопределения
        protected override void OnStartup(StartupEventArgs e)
        {
            SettingsManager.OnStartup();
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            SettingsManager.OnExit();
            base.OnExit(e);
        }

        #endregion Переопределения
    }
}
