﻿using DrawToolsLib;
using System;
using System.Windows;
using System.Windows.Media;
using Utilities;

namespace Paint_2._0
{
    public enum AvailableLanguage
    {
        Russian,
        English
    }

    /// <summary>
    /// Класс параметров приложения. 
    /// Содержит все постоянные параметры приложения
    /// </summary>
    public class Settings
    {
        #region Члены
        WindowStateInfo mainWindowStateInfo;
        MruInfo recentFilesList;
        string initialDirectory;
        double lineWidth;
        Color objectColor;
        string textFontFamilyName;
        string textFontStyle;
        string textFontWeight;
        string textFontStretch;
        double textFontSize;
        AvailableLanguage language;

        #endregion Члены

        #region Конструктор
        /// <summary>
        /// Создает экземпляры всех внутренних классов и устанавливает все значения по умолчанию
        /// </summary>
        public Settings()
        {
            mainWindowStateInfo = new WindowStateInfo();
            recentFilesList = new MruInfo();
            initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            lineWidth = 1;
            objectColor = Colors.Black;
            textFontFamilyName = "Tahoma";
            textFontStyle = FontConversions.FontStyleToString(FontStyles.Normal);
            textFontWeight = FontConversions.FontWeightToString(FontWeights.Normal);
            textFontStretch = FontConversions.FontStretchToString(FontStretches.Normal);
            textFontSize = 12;
        }

        #endregion Конструктор

        #region Свойства
        public WindowStateInfo MainWindowStateInfo
        {
            get { return mainWindowStateInfo; }
            set { mainWindowStateInfo = value; }
        }

        public MruInfo RecentFilesList
        {
            get { return recentFilesList; }
            set { recentFilesList = value; }
        }

        public string InitialDirectory
        {
            get { return initialDirectory; }
            set { initialDirectory = value; }
        }

        public double LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        public Color ObjectColor
        {
            get { return objectColor; }
            set { objectColor = value; }
        }

        public string TextFontFamilyName
        {
            get { return textFontFamilyName; }
            set { textFontFamilyName = value; }
        }

        public string TextFontStyle
        {
            get { return textFontStyle; }
            set { textFontStyle = value; }
        }

        public string TextFontWeight
        {
            get { return textFontWeight; }
            set { textFontWeight = value; }
        }

        public string TextFontStretch
        {
            get { return textFontStretch; }
            set { textFontStretch = value; }
        }

        public double TextFontSize
        {
            get { return textFontSize; }
            set { textFontSize = value; }
        }

        public AvailableLanguage CurrentLanguage
        {
            get { return language; }
            set { language = value; }
        }

        #endregion Свойства
    }
}

