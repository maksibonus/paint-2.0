﻿using DrawToolsLib;
using Microsoft.Win32;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Utilities;

namespace Paint_2._0
{
    /// <summary>
    /// Главное окно программы
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        #region Члены
        WindowStateManager windowStateManager;
        MruManager mruManager;

        // имя текущего файла
        string fileName;

        #endregion Члены

        #region Конструктор
        public MainWindow()
        {
            // Создаем WindowStateManager и связываем его с ApplicationSettings.MainWindowStateInfo.
            // Когда приложение закрывается, ApplicationSettings сохраняется с новым состоянием окна в XML
            windowStateManager = new WindowStateManager(SettingsManager.ApplicationSettings.MainWindowStateInfo, this);
            InitializeComponent();
            SubscribeToEvents();
            UpdateTitle();
            InitializeDrawingCanvas();
            InitializePropertiesControls();
            InitializeMruList();
        }

        #endregion Конструктор

        #region Команды приложения
        /// <summary>
        /// Новый файл
        /// </summary>
        void FileNewCommand(object sender, ExecutedRoutedEventArgs args)
        {
            if (!PromptToSave())
            {
                return;
            }
            drawingCanvas.Clear();
            fileName = "";
            UpdateTitle();
        }

        /// <summary>
        /// Печать
        /// </summary>
        void FilePrintCommand(object sender, ExecutedRoutedEventArgs args)
        {
            PrintWithoutBackground();
        }

        /// <summary>
        /// Выход
        /// </summary>
        void FileCloseCommand(object sender, ExecutedRoutedEventArgs args)
        {
            this.Close();
        }

        /// <summary>
        /// Открыть файл
        /// </summary>
        void FileOpenCommand(object sender, ExecutedRoutedEventArgs args)
        {
            if (!PromptToSave())
            {
                return;
            }
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "XML files (*.xml)|*.xml|All Files|*.*";
            dlg.DefaultExt = "xml";
            dlg.InitialDirectory = SettingsManager.ApplicationSettings.InitialDirectory;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog().GetValueOrDefault() != true)
            {
                return;
            }
            try
            {
                drawingCanvas.Load(dlg.FileName);
            }
            catch (DrawingCanvasException e)
            {
                ShowError(e.Message);
                mruManager.Delete(dlg.FileName);
                return;
            }
            this.fileName = dlg.FileName;
            UpdateTitle();
            mruManager.Add(this.fileName);
            // Запоминаем каталог
            SettingsManager.ApplicationSettings.InitialDirectory = System.IO.Path.GetDirectoryName(dlg.FileName);
        }

        /// <summary>
        /// Сохранить
        /// </summary>
        void FileSaveCommand(object sender, ExecutedRoutedEventArgs args)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                FileSaveAsCommand(sender, args);
                return;
            }
            Save(fileName);
        }

        /// <summary>
        /// Сохранить как
        /// </summary>
        void FileSaveAsCommand(object sender, ExecutedRoutedEventArgs args)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XML files (*.xml)|*.xml|All Files|*.*";
            dlg.OverwritePrompt = true;
            dlg.DefaultExt = "xml";
            dlg.InitialDirectory = SettingsManager.ApplicationSettings.InitialDirectory;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog().GetValueOrDefault() != true)
            {
                return;
            }
            if (!Save(dlg.FileName))
            {
                return;
            }
            // Запоминаем каталог
            SettingsManager.ApplicationSettings.InitialDirectory = System.IO.Path.GetDirectoryName(dlg.FileName);
        }

        /// <summary>
        /// Отменить
        /// </summary>
        void EditUndoCommand(object sender, ExecutedRoutedEventArgs args)
        {
            drawingCanvas.Undo();
        }

        /// <summary>
        /// Повторить
        /// </summary>
        void EditRedoCommand(object sender, ExecutedRoutedEventArgs args)
        {
            drawingCanvas.Redo();
        }

        #endregion Команды приложения

        #region Инструменты обработчиков событий

        /// <summary>
        /// Один из пунктов меню инструментов нажат
        /// </summary>
        void ToolMenuItem_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.Tool = (ToolType)Enum.Parse(typeof(ToolType), ((MenuItem)sender).Tag.ToString());
        }

        /// <summary>
        /// Одна из кнопок панели инструментов нажата.
        /// </summary>
        void ToolButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            drawingCanvas.Tool = (ToolType)Enum.Parse(typeof(ToolType),
                ((System.Windows.Controls.Primitives.ButtonBase)sender).Tag.ToString());
            e.Handled = true;
        }

        /// <summary>
        /// Выбран 1 из языков
        /// </summary>
        void LanguageButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(((MenuItem)sender).Tag.ToString());
            InitializeComponent();
            e.Handled = true;
            
        }

        #endregion Инструменты обработчиков событий

        #region Обработчики событий редактирования
        void menuEditSelectAll_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.SelectAll();
        }

        void menuEditUnselectAll_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.UnselectAll();
        }

        void menuEditDelete_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.Delete();
        }

        void menuEditDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.DeleteAll();
        }

        void menuEditMoveToFront_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.MoveToFront();
        }

        void menuEditMoveToBack_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.MoveToBack();
        }

        void menuEditSetProperties_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.SetProperties();
        }

        #endregion Обработчики событий редактирования

        #region Свойства обработчиков событий
        /// <summary>
        /// Показать диалог выбора шрифтов
        /// </summary>
        void PropertiesFont_Click(object sender, RoutedEventArgs e)
        {
            Petzold.ChooseFont.FontDialog dlg = new Petzold.ChooseFont.FontDialog();
            dlg.Owner = this;
            dlg.Background = SystemColors.ControlBrush;
            dlg.Title = "Select Font";
            dlg.FaceSize = drawingCanvas.TextFontSize;
            dlg.Typeface = new Typeface(
                new FontFamily(drawingCanvas.TextFontFamilyName),
                drawingCanvas.TextFontStyle,
                drawingCanvas.TextFontWeight,
                drawingCanvas.TextFontStretch);
            if (dlg.ShowDialog().GetValueOrDefault() != true)
            {
                return;
            }

            // Установливаем новый шрифт на полотно
            drawingCanvas.TextFontSize = dlg.FaceSize;
            drawingCanvas.TextFontFamilyName = dlg.Typeface.FontFamily.ToString();
            drawingCanvas.TextFontStyle = dlg.Typeface.Style;
            drawingCanvas.TextFontWeight = dlg.Typeface.Weight;
            drawingCanvas.TextFontStretch = dlg.Typeface.Stretch;

            // Установливаем новый шрифт в настройки
            SettingsManager.ApplicationSettings.TextFontSize = dlg.FaceSize;
            SettingsManager.ApplicationSettings.TextFontFamilyName = dlg.Typeface.FontFamily.ToString();
            SettingsManager.ApplicationSettings.TextFontStyle = FontConversions.FontStyleToString(dlg.Typeface.Style);
            SettingsManager.ApplicationSettings.TextFontWeight = FontConversions.FontWeightToString(dlg.Typeface.Weight);
            SettingsManager.ApplicationSettings.TextFontStretch = FontConversions.FontStretchToString(dlg.Typeface.Stretch);
        }

        /// <summary>
        /// Диалоговое окно выбора цвета
        /// </summary>
        void PropertiesColor_Click(object sender, RoutedEventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.Owner = this;
            dlg.Color = drawingCanvas.ObjectColor;

            if (dlg.ShowDialog().GetValueOrDefault() != true)
            {
                return;
            }
            drawingCanvas.ObjectColor = dlg.Color;
            SettingsManager.ApplicationSettings.ObjectColor = drawingCanvas.ObjectColor;
        }

        /// <summary>
        /// Изменена ширина линии
        /// </summary>
        void PropertiesLineWidth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            double lineWidth = Double.Parse(comboPropertiesLineWidth.SelectedValue.ToString(),
                CultureInfo.InvariantCulture);
            drawingCanvas.LineWidth = lineWidth;
            SettingsManager.ApplicationSettings.LineWidth = lineWidth;
        }

        #endregion Свойства обработчиков событий

        #region Другие обработчики событий
        /// <summary>
        /// Файл выбранный из MRU листа
        /// </summary>
        void mruManager_FileSelected(object sender, MruFileOpenEventArgs e)
        {
            if (!PromptToSave())
            {
                return;
            }
            try
            {
                drawingCanvas.Load(e.FileName);
            }
            catch (DrawingCanvasException ex)
            {
                ShowError(ex.Message);
                mruManager.Delete(e.FileName);

                return;
            }
            this.fileName = e.FileName;
            UpdateTitle();
            mruManager.Add(this.fileName);
        }

        /// <summary>
        /// При движении мыши по полотну
        /// </summary>
        private void drawingCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point pt = e.GetPosition(drawingCanvas);
            lblCoordinateInfo.Content = String.Format("X: {0}, Y: {1}", (int)pt.X, (int)pt.Y);

            //LostMouseCapture+=new MouseEventHandler(drawingCanvas_LostMouseCapture);
        }

        /// <summary>
        /// Данные сохранены
        /// </summary>
        void drawingCanvas_IsDirtyChanged(object sender, RoutedEventArgs e)
        {
            UpdateTitle();
        }

        /// <summary>
        /// Проверяет пункты меню инструментов, согласно активному инструменту
        /// </summary>
        void menuTools_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            menuToolsPointer.IsChecked = (drawingCanvas.Tool == ToolType.Pointer);
            menuToolsRectangle.IsChecked = (drawingCanvas.Tool == ToolType.Rectangle);
            menuToolsEllipse.IsChecked = (drawingCanvas.Tool == ToolType.Ellipse);
            menuToolsLine.IsChecked = (drawingCanvas.Tool == ToolType.Line);
            menuToolsPencil.IsChecked = (drawingCanvas.Tool == ToolType.PolyLine);
            menuToolsText.IsChecked = (drawingCanvas.Tool == ToolType.Text);
        }

        /// <summary>
        /// Включает редактирование пунктов меню в соответствии с DrawingCanvas
        /// </summary>
        void menuEdit_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            menuEditDelete.IsEnabled = drawingCanvas.CanDelete;
            menuEditDeleteAll.IsEnabled = drawingCanvas.CanDeleteAll;
            menuEditMoveToBack.IsEnabled = drawingCanvas.CanMoveToBack;
            menuEditMoveToFront.IsEnabled = drawingCanvas.CanMoveToFront;
            menuEditSelectAll.IsEnabled = drawingCanvas.CanSelectAll;
            menuEditUnselectAll.IsEnabled = drawingCanvas.CanUnselectAll;
            menuEditSetProperties.IsEnabled = drawingCanvas.CanSetProperties;
            menuEditUndo.IsEnabled = drawingCanvas.CanUndo;
            menuEditRedo.IsEnabled = drawingCanvas.CanRedo;
        }

        /// <summary>
        /// Проверяет пункты меню инструментов, согласно активному инструменту
        /// </summary>
        void menuLanguage_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            menuLanguageRussian.IsChecked = (SettingsManager.ApplicationSettings.CurrentLanguage == AvailableLanguage.Russian);
            menuLanguageEnglish.IsChecked = (SettingsManager.ApplicationSettings.CurrentLanguage == AvailableLanguage.English);
        }



        /// <summary>
        /// Окно "Об авторах"
        /// </summary>
        void HelpCommand(object sender, ExecutedRoutedEventArgs args)
        {
            AboutWindow dlg = new AboutWindow();
            dlg.Owner = this;

            try
            {
                dlg.ShowDialog();
            }
            catch (System.Net.WebException e)
            {
                // Нажатие на гиперссылку без подключения к Интернету
                if (dlg != null)
                {
                    dlg.Close();
                }

                ShowError(e.Message);
            }
        }

        /// <summary>
        /// Вопрос о сохранении при закрытии программы
        /// </summary>
        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!PromptToSave())
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Функция выполняет различные действия в зависимости от XAML версии
        /// </summary>
        void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Object o = FindName("imageBackground");
            if (o == null)
            {
                drawingCanvas.RefreshClip();
                return;
            }
            Image image = o as Image;
            if (image == null)
            {
                return;
            }
            o = FindName("viewBoxContainer");
            if (o == null)
            {
                return;
            }
            Viewbox v = o as Viewbox;
            if (v == null)
            {
                return;
            }

            // Вычисляется фактический масштаб изображения, нарисованного на экране.
            double viewBoxWidth = v.ActualWidth;
            double viewBoxHeight = v.ActualHeight;
            double imageWidth = image.Source.Width;
            double imageHeight = image.Source.Height;
            double scale;
            if (viewBoxWidth / imageWidth > viewBoxHeight / imageHeight)
            {
                scale = viewBoxWidth / imageWidth;
            }
            else
            {
                scale = viewBoxHeight / imageHeight;
            }

            // Применяем фактический масштаб холста
            drawingCanvas.ActualScale = scale;
        }

        #endregion Другие обработчики событий

        #region Другие функции
        /// <summary>
        /// Напоминание про сохранение. true - новый файл без сохранения
        /// </summary>
        bool PromptToSave()
        {
            if (!drawingCanvas.IsDirty)
            {
                return true;
            }
            MessageBoxResult result = MessageBox.Show(
                this,
                "Do you want to save changes?",
                Properties.Resources.ApplicationTitle,
                MessageBoxButton.YesNoCancel,
                MessageBoxImage.Question,
                MessageBoxResult.Yes);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    FileSaveCommand(null, null);
                    return (!drawingCanvas.IsDirty);
                case MessageBoxResult.No:
                    return true;
                case MessageBoxResult.Cancel:
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Подписывает на различные события
        /// </summary>
        void SubscribeToEvents()
        {
            this.SizeChanged += new SizeChangedEventHandler(MainWindow_SizeChanged);
            this.Closing += new System.ComponentModel.CancelEventHandler(MainWindow_Closing);
            drawingCanvas.IsDirtyChanged += new RoutedEventHandler(drawingCanvas_IsDirtyChanged);

            // При открытом меню - используется для установки состояния пунктов меню 
            menuTools.SubmenuOpened += new RoutedEventHandler(menuTools_SubmenuOpened);
            menuEdit.SubmenuOpened += new RoutedEventHandler(menuEdit_SubmenuOpened);
            menuLanguage.SubmenuOpened += new RoutedEventHandler(menuLanguage_SubmenuOpened);

            // Меню инструментов
            menuToolsPointer.Click += ToolMenuItem_Click;
            menuToolsRectangle.Click += ToolMenuItem_Click;
            menuToolsEllipse.Click += ToolMenuItem_Click;
            menuToolsLine.Click += ToolMenuItem_Click;
            menuToolsPencil.Click += ToolMenuItem_Click;
            menuToolsText.Click += ToolMenuItem_Click;

            // Кнопки инструментов
            buttonToolPointer.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);
            buttonToolRectangle.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);
            buttonToolEllipse.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);
            buttonToolLine.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);
            buttonToolPencil.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);
            buttonToolText.PreviewMouseDown += new MouseButtonEventHandler(ToolButton_PreviewMouseDown);

            // Меню редактирования
            menuEditSelectAll.Click += new RoutedEventHandler(menuEditSelectAll_Click);
            menuEditUnselectAll.Click += new RoutedEventHandler(menuEditUnselectAll_Click);
            menuEditDelete.Click += new RoutedEventHandler(menuEditDelete_Click);
            menuEditDeleteAll.Click += new RoutedEventHandler(menuEditDeleteAll_Click);
            menuEditMoveToFront.Click += new RoutedEventHandler(menuEditMoveToFront_Click);
            menuEditMoveToBack.Click += new RoutedEventHandler(menuEditMoveToBack_Click);
            menuEditSetProperties.Click += new RoutedEventHandler(menuEditSetProperties_Click);

            //Меню языков
            menuLanguageRussian.PreviewMouseDown += new MouseButtonEventHandler(LanguageButton_PreviewMouseDown);
            menuLanguageEnglish.PreviewMouseDown += new MouseButtonEventHandler(LanguageButton_PreviewMouseDown);
        }


        /// <summary>
        /// Инициализирирует свойства элементов управления на панели инструментов
        /// </summary>
        void InitializePropertiesControls()
        {
            for (int i = 1; i <= 10; i++)
            {
                comboPropertiesLineWidth.Items.Add(i.ToString(CultureInfo.InvariantCulture));
            }
            int lineWidth = (int)(drawingCanvas.LineWidth + 0.5);
            if (lineWidth < 1)
            {
                lineWidth = 1;
            }
            if (lineWidth > 10)
            {
                lineWidth = 10;
            }
            comboPropertiesLineWidth.SelectedIndex = lineWidth - 1;
            buttonPropertiesFont.Click += new RoutedEventHandler(PropertiesFont_Click);
            buttonPropertiesColor.Click += new RoutedEventHandler(PropertiesColor_Click);
            comboPropertiesLineWidth.SelectionChanged += new SelectionChangedEventHandler(PropertiesLineWidth_SelectionChanged);
        }

        /// <summary>
        /// Инициалитирует MRU лист
        /// </summary>
        void InitializeMruList()
        {
            mruManager = new MruManager(SettingsManager.ApplicationSettings.RecentFilesList, menuFileRecentFiles);
            mruManager.FileSelected += new EventHandler<MruFileOpenEventArgs>(mruManager_FileSelected);
        }


        /// <summary>
        /// Устанавливает первоначальные свойства полотно
        /// </summary>
        void InitializeDrawingCanvas()
        {
            drawingCanvas.LineWidth = SettingsManager.ApplicationSettings.LineWidth;
            drawingCanvas.ObjectColor = SettingsManager.ApplicationSettings.ObjectColor;

            drawingCanvas.TextFontSize = SettingsManager.ApplicationSettings.TextFontSize;
            drawingCanvas.TextFontFamilyName = SettingsManager.ApplicationSettings.TextFontFamilyName;
            drawingCanvas.TextFontStyle = FontConversions.FontStyleFromString(SettingsManager.ApplicationSettings.TextFontStyle);
            drawingCanvas.TextFontWeight = FontConversions.FontWeightFromString(SettingsManager.ApplicationSettings.TextFontWeight);
            drawingCanvas.TextFontStretch = FontConversions.FontStretchFromString(SettingsManager.ApplicationSettings.TextFontStretch);
        }

        /// <summary>
        /// Показывает сообщения об ошибках
        /// </summary>
        void ShowError(string message)
        {
            MessageBox.Show(
                this,
                message,
                Properties.Resources.ApplicationTitle,
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }

        /// <summary>
        /// Обновляет заголовок окна
        /// </summary>
        void UpdateTitle()
        {
            string s = Properties.Resources.ApplicationTitle + " - ";
            if (string.IsNullOrEmpty(fileName))
            {
                s += Properties.Resources.Untitled;
            }
            else
            {
                s += System.IO.Path.GetFileName(fileName);
            }
            if (drawingCanvas.IsDirty)
            {
                s += " *";
            }
            this.Title = s;
        }

        /// <summary>
        /// Сохраняет в файл
        /// </summary>
        bool Save(string file)
        {
            try
            {
                drawingCanvas.Save(file);
            }
            catch (DrawingCanvasException e)
            {
                ShowError(e.Message);
                return false;
            }
            this.fileName = file;
            UpdateTitle();
            mruManager.Add(this.fileName);
            return true;
        }

        /// <summary>
        /// Выводит графику
        /// </summary>
        void PrintWithoutBackground()
        {
            PrintDialog dlg = new PrintDialog();

            if (dlg.ShowDialog().GetValueOrDefault() != true)
            {
                return;
            }
            double width = dlg.PrintableAreaWidth / 2;
            double height = width * drawingCanvas.ActualHeight / drawingCanvas.ActualWidth;
            double left = (dlg.PrintableAreaWidth - width) / 2;
            double top = (dlg.PrintableAreaHeight - height) / 2;
            Rect rect = new Rect(left, top, width, height);
            DrawingVisual vs = new DrawingVisual();
            DrawingContext dc = vs.RenderOpen();
            double scale = width / drawingCanvas.ActualWidth;

            // Хранит старые фактические масштабы и установливает новые фактические масштабы
            double oldActualScale = drawingCanvas.ActualScale;
            drawingCanvas.ActualScale = scale;

            drawingCanvas.RemoveClip();
            dc.DrawRectangle(null, new Pen(Brushes.Black, 1),
                new Rect(rect.Left - 1, rect.Top - 1, rect.Width + 2, rect.Height + 2));

            // Подготовливаем DrawingContext к рисованию графики
            dc.PushClip(new RectangleGeometry(rect));
            dc.PushTransform(new TranslateTransform(left, top));
            dc.PushTransform(new ScaleTransform(scale, scale));

            // Рисуем все слои
            drawingCanvas.Draw(dc);

            // Возвращаем старые размеры
            drawingCanvas.ActualScale = oldActualScale;

            // Восстанавливаем clip-ы
            drawingCanvas.RefreshClip();

            dc.Pop();
            dc.Pop();
            dc.Pop();
            dc.Close();
            dlg.PrintVisual(vs, "Graphics");
        }

        #endregion Другие функции

        private void sliderScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int width = 1406;
            int height = 630;
            drawingCanvas.Width = width * sliderScale.Value;
            drawingCanvas.Height = height * sliderScale.Value;
        }
    }
}
