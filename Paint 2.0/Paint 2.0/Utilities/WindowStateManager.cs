﻿using System;
using System.Windows;

namespace Utilities
{
    /// <summary>
    /// Класс ответственный за изменение состояния окна.
    /// Создается до инициализации окна
    /// </summary>
    public class WindowStateManager
    {
        WindowStateInfo windowStateInfo;
        Window ownerWindow;

        public WindowStateInfo WindowStateInfo
        {
            get { return windowStateInfo; }
            set { windowStateInfo = value; }
        }

        public Window OwnerForm
        {
            get { return ownerWindow; }
            set { ownerWindow = value; }
        }

        public WindowStateManager(WindowStateInfo windowStateInfo, Window ownerWindow)
        {
            if (windowStateInfo == null)
            {
                throw new ArgumentNullException("windowStateInfo");
            }
            if (ownerWindow == null)
            {
                throw new ArgumentNullException("ownerWindow");
            }
            this.windowStateInfo = windowStateInfo;
            this.ownerWindow = ownerWindow;

            // Внутренее окно подписывается на владельца окна
            ownerWindow.LocationChanged += new EventHandler(ownerWindow_LocationChanged);
            ownerWindow.SizeChanged += new SizeChangedEventHandler(ownerWindow_SizeChanged);
            SetInitialWindowState();
        }

        /// <summary>
        /// Установка начального состояния окна
        /// </summary>
        private void SetInitialWindowState()
        {
            // Если не задано начальное состояние, пользователь опеределяет его
            if (windowStateInfo.Width == 0.0 && windowStateInfo.Height == 0.0)
            {
                return;
            }
            double minSize = 50.0;

            // Проверяем windowStateInfo на валидность
            if (windowStateInfo.Width < ownerWindow.MinWidth)
                windowStateInfo.Width = ownerWindow.MinWidth;
            if (windowStateInfo.Width > ownerWindow.MaxWidth)
                windowStateInfo.Width = ownerWindow.MaxWidth;
            if (windowStateInfo.Height < ownerWindow.MinHeight)
                windowStateInfo.Height = ownerWindow.MinHeight;
            if (windowStateInfo.Height > ownerWindow.MaxHeight)
                windowStateInfo.Height = ownerWindow.MaxHeight;
            if (windowStateInfo.Left < 0.0)
                windowStateInfo.Left = 0.0;
            if (windowStateInfo.Left > SystemParameters.WorkArea.Width - minSize)
                windowStateInfo.Left = (SystemParameters.WorkArea.Width - minSize);
            if (windowStateInfo.Top < 0.0)
                windowStateInfo.Top = 0.0;
            if (windowStateInfo.Top > SystemParameters.WorkArea.Height - minSize)
                windowStateInfo.Top = (SystemParameters.WorkArea.Height - minSize);

            // Устанавливаем параметры окна
            ownerWindow.Left = windowStateInfo.Left;
            ownerWindow.Top = windowStateInfo.Top;
            ownerWindow.Width = windowStateInfo.Width;
            ownerWindow.Height = windowStateInfo.Height;
            if (windowStateInfo.State == WindowState.Normal || windowStateInfo.State == WindowState.Maximized)
            {
                ownerWindow.WindowState = windowStateInfo.State;
            }
        }

        /// <summary>
        /// Отслеживает положение окна
        /// </summary>
        void ownerWindow_LocationChanged(object sender, EventArgs e)
        {
            // save position
            if (ownerWindow.WindowState == WindowState.Normal)
            {
                windowStateInfo.Left = ownerWindow.Left;
                windowStateInfo.Top = ownerWindow.Top;
            }
        }

        /// <summary>
        /// Отслеживает размеры окна
        /// </summary>
        void ownerWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // Сохраняем ширину и высоту окна
            if (ownerWindow.WindowState == WindowState.Normal)
            {
                windowStateInfo.Width = ownerWindow.Width;
                windowStateInfo.Height = ownerWindow.Height;
            }
            windowStateInfo.State = ownerWindow.WindowState;
        }
    }
}

