﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Utilities
{
    /// <summary>
    /// Конвертирует размер шрифта в строку
    /// </summary>
    [ValueConversion(typeof(double), typeof(string))]
    public class FontSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            double d = (double)value * 0.75;

            return ((int)(d + 0.5)).ToString(CultureInfo.InvariantCulture);
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return new NotSupportedException(this.GetType().Name + Properties.Settings.Default.ConvertBackNotSupported);
        }
    }
}

