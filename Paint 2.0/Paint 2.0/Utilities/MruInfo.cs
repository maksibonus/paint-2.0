﻿namespace Utilities
{
    /// <summary>
    /// Хранит информацию о списке файлов для MRU
    /// </summary>
    public class MruInfo
    {
        string[] recentFiles;

        public MruInfo()
        {
            recentFiles = new string[0];
        }

        /// <summary>
        /// Лист имен файлов
        /// </summary>
        public string[] RecentFiles
        {
            get { return recentFiles; }
            set { recentFiles = value; }
        }
    }
}

