﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Utilities
{
    /// <summary>
    /// Класс, управляющий файлами
    /// </summary>
    public class MruManager
    {
        #region Члены
        MruInfo infoMru;
        MenuItem menuItemMru;
        int maxLength = 10;

        public event EventHandler<MruFileOpenEventArgs> FileSelected;

        #endregion Члены

        #region Конструктор
        public MruManager(MruInfo mruInfo, MenuItem mruMenuItem)
        {
            if (mruInfo == null)
            {
                throw new ArgumentNullException("mruInfo");
            }
            if (mruMenuItem == null)
            {
                throw new ArgumentNullException("mruMenuItem");
            }
            this.infoMru = mruInfo;
            this.menuItemMru = mruMenuItem;
            UpdateMenu();
        }

        #endregion Конструктор

        #region Свойства
        /// <summary>
        /// Максимальное количество файлов
        /// </summary>
        public int MaxLength
        {
            get
            {
                return maxLength;
            }
            set
            {
                if (value > 0)
                {
                    maxLength = value;
                }
            }
        }

        #endregion Свойства

        #region Открытые функции
        /// <summary>
        /// Добавляет файлы в MRU лист
        /// </summary>
        public void Add(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            List<string> list = new List<string>();
            list.Add(fileName);
            if (infoMru.RecentFiles != null)
            {
                foreach (string s in infoMru.RecentFiles)
                {
                    if (s != fileName)
                    {
                        list.Add(s);
                    }
                }
            }
            MakeArrayFromList(list);
            UpdateMenu();
        }

        /// <summary>
        /// Удаление имени файла из MRU листа.
        /// Эта функция вызывается после каждой неудачной операции открытия
        /// </summary>
        public void Delete(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
            List<string> list = new List<string>();

            if (infoMru.RecentFiles != null)
            {
                foreach (string s in infoMru.RecentFiles)
                {
                    if (s != fileName)
                    {
                        list.Add(s);
                    }
                }
            }
            MakeArrayFromList(list);
            UpdateMenu();
        }

        #endregion Открытые функции

        #region Другие функции
        /// <summary>
        /// Заполняет MRU меню
        /// </summary>
        void UpdateMenu()
        {
            menuItemMru.Items.Clear();
            if (infoMru.RecentFiles != null)
            {
                foreach (string fullName in infoMru.RecentFiles)
                {
                    MenuItem item = new MenuItem();
                    string shortName = System.IO.Path.GetFileName(fullName);
                    ToolTip toolTip = new ToolTip();
                    toolTip.Content = fullName;

                    item.Header = shortName;
                    item.ToolTip = toolTip;
                    item.Tag = fullName;
                    item.Click += new RoutedEventHandler(item_Click);
                    menuItemMru.Items.Add(item);
                }
            }
            menuItemMru.IsEnabled = (infoMru.RecentFiles.Length > 0);
        }


        /// <summary>
        /// MRU элемент выбран, оповещаем пользователя
        /// </summary>
        void item_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            string fileName = (string)item.Tag;

            if (FileSelected != null)
            {
                FileSelected(this, new MruFileOpenEventArgs(fileName));
            }
        }

        /// <summary>
        /// Создает лист файлов и сохраняет его
        /// </summary>
        void MakeArrayFromList(List<string> list)
        {
            string[] files = new string[Math.Min(list.Count, maxLength)];

            int index = 0;

            foreach (string s in list)
            {
                files[index++] = s;

                if (index >= maxLength)
                {
                    break;
                }
            }
            infoMru.RecentFiles = files;
        }

        #endregion Другие функции
    }
}

