﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;


namespace Utilities
{
    /// <summary>
    /// Класс, содержащий сведения о состоянии окна.
    /// Передается в WindowStateManager
    /// </summary>
    public class WindowStateInfo
    {
        // Параметры окна
        double left;
        double top;
        double width;
        double height;

        // Состояение окна
        WindowState state;

        public WindowStateInfo()
        {
            state = WindowState.Normal;
        }

        public double Left
        {
            get { return left; }
            set { left = value; }
        }

        public double Top
        {
            get { return top; }
            set { top = value; }
        }

        public double Width
        {
            get { return width; }
            set { width = value; }
        }

        public double Height
        {
            get { return height; }
            set { height = value; }
        }

        public WindowState State
        {
            get { return state; }
            set { state = value; }
        }
    }
}

