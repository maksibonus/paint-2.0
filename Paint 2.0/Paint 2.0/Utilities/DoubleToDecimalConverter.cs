﻿//---------------------------------------------------------
// DoubleToDecimalConverter.cs (c) 2006 by Charles Petzold
//---------------------------------------------------------
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Petzold.Converters
{
    /// <summary>
    /// Конвертирует double в decimal
    /// Пример использования в XAML:
    /// Binding Path=..., Converter={StaticResource converter}, ConverterParameter=2}
    /// </summary>
    [ValueConversion(typeof(double), typeof(decimal))]
    public class DoubleToDecimalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            decimal num = new Decimal((double)value);

            if (parameter != null) 
                num = Decimal.Round(num, int.Parse(parameter as string, CultureInfo.InvariantCulture));

            return num;
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return Decimal.ToDouble((decimal)value);
        }
    }
}

