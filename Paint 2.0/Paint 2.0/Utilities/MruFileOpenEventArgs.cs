﻿namespace Utilities
{
    /// <summary>
    /// Аргументы события FileSelected для вызова из MruManager.
    /// </summary>
    public class MruFileOpenEventArgs : System.EventArgs
    {
        private string fileName;

        public MruFileOpenEventArgs(string fileName)
        {
            this.fileName = fileName;
        }

        public string FileName
        {
            get
            {
                return fileName;
            }
        }
    }
}

