﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Utilities
{
    /// <summary>
    /// Конвертирует double в integer
    /// </summary>
    [ValueConversion(typeof(double), typeof(decimal))]
    public class DoubleToIntegerConverter : IValueConverter
    {
        int min = int.MinValue;
        int max = int.MaxValue;

        #region Свойства
        /// <summary>
        /// Минимальное значение 
        /// </summary>
        public int Min
        {
            get { return min; }
            set { min = value; }
        }

        /// <summary>
        /// Максимальное значение
        /// </summary>
        public int Max
        {
            get { return max; }
            set { max = value; }
        }

        #endregion Свойства

        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            int result = (int)((double)value + 0.5);
            if (result < min)
            {
                result = min;
            }
            if (result > max)
            {
                result = max;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return null;
        }

    }
}

