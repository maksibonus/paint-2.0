﻿using System;
using System.Windows;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    ///  Графический объект текста
    /// </summary>
    public class GraphicsText : GraphicsRectangleBase
    {
        #region Члены
        private string text;
        private string textFontFamilyName;
        private FontStyle textFontStyle;
        private FontWeight textFontWeight;
        private FontStretch textFontStretch;
        private double textFontSize;
        FormattedText formattedText;

        #endregion Члены

        #region Конструкторы
        public GraphicsText(
            string text,
            double left,
            double top,
            double right,
            double bottom,
            Color objectColor,
            double textFontSize,
            string textFontFamilyName,
            FontStyle textFontStyle,
            FontWeight textFontWeight,
            FontStretch textFontStretch,
            double actualScale)
        {
            this.text = text;
            this.rectangleLeft = left;
            this.rectangleTop = top;
            this.rectangleRight = right;
            this.rectangleBottom = bottom;
            this.graphicsObjectColor = objectColor;
            this.textFontSize = textFontSize;
            this.textFontFamilyName = textFontFamilyName;
            this.textFontStyle = textFontStyle;
            this.textFontWeight = textFontWeight;
            this.textFontStretch = textFontStretch;
            this.graphicsActualScale = actualScale;
            graphicsLineWidth = 2;
        }

        public GraphicsText()
            : this(Properties.Settings.Default.UnknownText, 0, 0, 0, 0, Colors.Black, 12,
                 Properties.Settings.Default.DefaultFontFamily, FontStyles.Normal,
                 FontWeights.Normal, FontStretches.Normal, 1.0)
        { }

        #endregion Конструкторы

        #region Свойства
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                RefreshDrawing();
            }
        }

        public string TextFontFamilyName
        {
            get
            {
                return textFontFamilyName;
            }
            set
            {
                textFontFamilyName = value;
                RefreshDrawing();
            }
        }

        public FontStyle TextFontStyle
        {
            get
            {
                return textFontStyle;
            }
            set
            {
                textFontStyle = value;
                RefreshDrawing();
            }
        }

        public FontWeight TextFontWeight
        {
            get
            {
                return textFontWeight;
            }
            set
            {
                textFontWeight = value;
                RefreshDrawing();
            }
        }

        public FontStretch TextFontStretch
        {
            get
            {
                return textFontStretch;
            }
            set
            {
                textFontStretch = value;
                RefreshDrawing();
            }
        }

        public double TextFontSize
        {
            get
            {
                return textFontSize;
            }
            set
            {
                textFontSize = value;
                RefreshDrawing();
            }
        }

        #endregion Свойства

        #region Переопределения
        public override void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException("drawingContext");
            }
            CreateFormattedText();
            Rect rect = Rectangle;
            drawingContext.PushClip(new RectangleGeometry(rect));
            drawingContext.DrawText(formattedText, new Point(rect.Left, rect.Top));
            drawingContext.Pop();
            if (IsSelected)
            {
                drawingContext.DrawRectangle(
                    null,
                    new Pen(new SolidColorBrush(graphicsObjectColor), ActualLineWidth),
                    rect);
            }
            base.Draw(drawingContext);
        }

        public override bool Contains(Point point)
        {
            return this.Rectangle.Contains(point);
        }

        public override PropertiesGraphicsBase CreateSerializedObject()
        {
            return new PropertiesGraphicsText(this);
        }

        #endregion Переопределения

        #region Другие методы
        /// <summary>
        /// Создает форматированный текст
        /// </summary>
        void CreateFormattedText()
        {
            if (String.IsNullOrEmpty(textFontFamilyName))
            {
                textFontFamilyName = Properties.Settings.Default.DefaultFontFamily;
            }
            if (text == null)
            {
                text = "";
            }
            if (textFontSize <= 0.0)
            {
                textFontSize = 12.0;
            }
            Typeface typeface = new Typeface(
                new FontFamily(textFontFamilyName),
                textFontStyle,
                textFontWeight,
                textFontStretch);
            formattedText = new FormattedText(
                text,
                System.Globalization.CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                typeface,
                textFontSize,
                new SolidColorBrush(graphicsObjectColor));
            formattedText.MaxTextWidth = Rectangle.Width;
        }

        #endregion Другие методы
    }
}
