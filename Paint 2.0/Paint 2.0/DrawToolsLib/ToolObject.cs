﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Базовый класс для всех инструментов, создающих графические объекты
    /// </summary>
    abstract class ToolObject : Tool
    {
        private Cursor toolCursor;

        /// <summary>
        /// Инструмент курсора
        /// </summary>
        protected Cursor ToolCursor
        {
            get
            {
                return toolCursor;
            }
            set
            {
                toolCursor = value;
            }
        }


        /// <summary>
        /// Создание нового объекта при отпускании мыши
        /// </summary>
        public override void OnMouseUp(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            if (drawingCanvas.Count > 0)
            {
                drawingCanvas[drawingCanvas.Count - 1].Normalize();
                drawingCanvas.AddCommandToHistory(new CommandAdd(drawingCanvas[drawingCanvas.Count - 1]));
            }
            drawingCanvas.Tool = ToolType.Pointer;
            drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
            drawingCanvas.ReleaseMouseCapture();
        }

        /// <summary>
        /// Создание нового объекта
        /// </summary>
        protected static void AddNewObject(DrawingCanvas drawingCanvas, GraphicsBase o)
        {
            HelperFunctions.UnselectAll(drawingCanvas);
            o.IsSelected = true;
            o.Clip = new RectangleGeometry(new Rect(0, 0, drawingCanvas.ActualWidth, drawingCanvas.ActualHeight));
            drawingCanvas.GraphicsList.Add(o);
            drawingCanvas.CaptureMouse();
        }

        /// <summary>
        /// Установить курсор
        /// </summary>
        public override void SetCursor(DrawingCanvas drawingCanvas)
        {
            drawingCanvas.Cursor = this.toolCursor;
        }
    }
}

