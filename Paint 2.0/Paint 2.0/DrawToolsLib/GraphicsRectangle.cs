﻿using System;
using System.Windows;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Графический объект прямоугольника
    /// </summary>
    public class GraphicsRectangle : GraphicsRectangleBase
    {
        #region Конструкторы
        public GraphicsRectangle(double left, double top, double right, double bottom,
            double lineWidth, Color objectColor, double actualScale)
        {
            this.rectangleLeft = left;
            this.rectangleTop = top;
            this.rectangleRight = right;
            this.rectangleBottom = bottom;
            this.graphicsLineWidth = lineWidth;
            this.graphicsObjectColor = objectColor;
            this.graphicsActualScale = actualScale;
        }

        public GraphicsRectangle()
            : this(0.0, 0.0, 100.0, 100.0, 1.0, Colors.Black, 1.0)
        { }

        #endregion Конструкторы

        #region Переопределения
        public override void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException("drawingContext");
            }
            drawingContext.DrawRectangle(
                null,
                new Pen(new SolidColorBrush(ObjectColor), ActualLineWidth),
                Rectangle);
            base.Draw(drawingContext);
        }

        public override bool Contains(Point point)
        {
            return this.Rectangle.Contains(point);
        }

        public override PropertiesGraphicsBase CreateSerializedObject()
        {
            return new PropertiesGraphicsRectangle(this);
        }

        #endregion Переопределения
    }
}

