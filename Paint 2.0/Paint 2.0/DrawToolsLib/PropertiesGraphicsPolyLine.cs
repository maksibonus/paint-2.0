﻿using System;
using System.Windows;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Свойства ломанной линии
    /// </summary>
    public class PropertiesGraphicsPolyLine : PropertiesGraphicsBase
    {
        private Point[] points;
        private double lineWidth;
        private Color objectColor;

        public PropertiesGraphicsPolyLine()
        { }

        public PropertiesGraphicsPolyLine(GraphicsPolyLine polyLine)
        {
            if (polyLine == null)
            {
                throw new ArgumentNullException("polyLine");
            }
            points = polyLine.GetPoints();
            lineWidth = polyLine.LineWidth;
            objectColor = polyLine.ObjectColor;
            actualScale = polyLine.ActualScale;
            ID = polyLine.Id;
            selected = polyLine.IsSelected;
        }

        public override GraphicsBase CreateGraphics()
        {
            GraphicsBase b = new GraphicsPolyLine(points, lineWidth, objectColor, actualScale);
            if (this.ID != 0)
            {
                b.Id = this.ID;
                b.IsSelected = this.selected;
            }
            return b;

        }

        #region Свойства
        /// <summary>
        /// Точки
        /// </summary>
        public Point[] Points
        {
            get { return points; }
            set { points = value; }
        }

        /// <summary>
        /// Ширина линии
        /// </summary>
        public double LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        /// <summary>
        /// Цвет
        /// </summary>
        public Color ObjectColor
        {
            get { return objectColor; }
            set { objectColor = value; }
        }

        #endregion Свойства
    }
}

