﻿using System.Windows.Input;

namespace DrawToolsLib
{
    /// <summary>
    /// Базовый класс для прямоугольника и эллипса
    /// </summary>
    abstract class ToolRectangleBase : ToolObject
    {
        /// <summary>
        /// Устанавливает курсор и меняет размер
        /// </summary>
        public override void OnMouseMove(DrawingCanvas drawingCanvas, MouseEventArgs e)
        {
            drawingCanvas.Cursor = ToolCursor;
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (drawingCanvas.IsMouseCaptured)
                {
                    if (drawingCanvas.Count > 0)
                    {
                        drawingCanvas[drawingCanvas.Count - 1].MoveHandleTo(
                            e.GetPosition(drawingCanvas), 5);
                    }
                }
            }
        }
    }
}

