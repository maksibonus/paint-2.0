﻿using System;
using System.Collections.Generic;

namespace DrawToolsLib
{
    /// <summary>
    /// Класс ответственный за команды Отмена, Повтор
    /// </summary>
    class UndoManager
    {
        #region Члены
        DrawingCanvas drawingCanvas;
        List<CommandBase> historyList;
        int nextUndo;

        /// <summary>
        /// При любом изменении состояния MruManager
        /// </summary>
        public event EventHandler StateChanged;

        #endregion  Члены

        #region Конструктор
        public UndoManager(DrawingCanvas drawingCanvas)
        {
            this.drawingCanvas = drawingCanvas;
            ClearHistory();
        }

        #endregion Конструктор

        #region Properties

        /// <summary>
        /// Доступна ли операция Отмены
        /// </summary>
        public bool CanUndo
        {
            get
            {
                if (nextUndo < 0 ||
                    nextUndo > historyList.Count - 1)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Доступна ли операция Повтора
        /// </summary>
        public bool CanRedo
        {
            get
            {
                if (nextUndo == historyList.Count - 1)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Является ли файл не сохраненным
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return CanUndo;
            }
        }

        #endregion Свойства

        #region Открытые методы
        /// <summary>
        /// Очистка истории
        /// </summary>
        public void ClearHistory()
        {
            historyList = new List<CommandBase>();
            nextUndo = -1;
            RaiseStateChangedEvent();
        }

        /// <summary>
        /// Добавляет новую команду в историю
        /// </summary>
        public void AddCommandToHistory(CommandBase command)
        {
            this.TrimHistoryList();
            historyList.Add(command);
            nextUndo++;
            RaiseStateChangedEvent();
        }

        /// <summary>
        /// Отмена
        /// </summary>
        public void Undo()
        {
            if (!CanUndo)
            {
                return;
            }
            CommandBase command = historyList[nextUndo];
            command.Undo(drawingCanvas);
            nextUndo--;
            RaiseStateChangedEvent();
        }

        /// <summary>
        /// Повтор
        /// </summary>
        public void Redo()
        {
            if (!CanRedo)
            {
                return;
            }
            int itemToRedo = nextUndo + 1;
            CommandBase command = historyList[itemToRedo];
            command.Redo(drawingCanvas);
            nextUndo++;
            RaiseStateChangedEvent();
        }

        #endregion Открытые методы

        #region Скрытые методы
        /// <summary>
        /// Очищает все предыдущие команды
        /// </summary>
        private void TrimHistoryList()
        {
            if (historyList.Count == 0)
            {
                return;
            }

            if (nextUndo == historyList.Count - 1)
            {
                return;
            }

            for (int i = historyList.Count - 1; i > nextUndo; i--)
            {
                historyList.RemoveAt(i);
            }
        }

        /// <summary>
        /// Вызывает событие UndoManager
        /// </summary>
        private void RaiseStateChangedEvent()
        {
            if (StateChanged != null)
            {
                StateChanged(this, EventArgs.Empty);
            }
        }

        #endregion Скрытые методы
    }
}

