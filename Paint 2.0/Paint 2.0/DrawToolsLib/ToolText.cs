﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Инструмент поля текста
    /// </summary>
    class ToolText : ToolRectangleBase
    {
        TextBox textBox;
        string oldText;
        GraphicsText editedGraphicsText;
        DrawingCanvas drawingCanvas;

        public ToolText(DrawingCanvas drawingCanvas)
        {
            this.drawingCanvas = drawingCanvas;
            MemoryStream stream = new MemoryStream(Properties.Resources.Text);
            ToolCursor = new Cursor(stream);
        }

        public TextBox TextBox
        {
            get { return textBox; }
            set { textBox = value; }
        }

        /// <summary>
        /// Создает новый текстовый объект
        /// </summary>
        public override void OnMouseDown(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(drawingCanvas);
            AddNewObject(drawingCanvas,
                new GraphicsText(
                String.Empty,
                p.X,
                p.Y,
                p.X + 1,
                p.Y + 1,
                drawingCanvas.ObjectColor,
                drawingCanvas.TextFontSize,
                drawingCanvas.TextFontFamilyName,
                drawingCanvas.TextFontStyle,
                drawingCanvas.TextFontWeight,
                drawingCanvas.TextFontStretch,
                drawingCanvas.ActualScale));
        }

        /// <summary>
        /// Создает новый объект при отпускании мыши
        /// </summary>
        public override void OnMouseUp(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            drawingCanvas.Tool = ToolType.Pointer;
            drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
            drawingCanvas.ReleaseMouseCapture();
            if (drawingCanvas.Count > 0)
            {
                drawingCanvas[drawingCanvas.Count - 1].Normalize();
                GraphicsText t = drawingCanvas[drawingCanvas.Count - 1] as GraphicsText;
                if (t != null)
                {
                    CreateTextBox(t, drawingCanvas);
                }
            }
        }

        /// <summary>
        /// Создает новый текстовый блок
        /// </summary>
        public void CreateTextBox(GraphicsText graphicsText, DrawingCanvas drawingCanvas)
        {
            graphicsText.IsSelected = false;
            oldText = graphicsText.Text;
            editedGraphicsText = graphicsText;

            textBox = new TextBox();
            textBox.Width = graphicsText.Rectangle.Width;
            textBox.Height = graphicsText.Rectangle.Height;
            textBox.FontFamily = new FontFamily(graphicsText.TextFontFamilyName);
            textBox.FontSize = graphicsText.TextFontSize;
            textBox.FontStretch = graphicsText.TextFontStretch;
            textBox.FontStyle = graphicsText.TextFontStyle;
            textBox.FontWeight = graphicsText.TextFontWeight;
            textBox.Text = graphicsText.Text;
            textBox.AcceptsReturn = true;
            textBox.TextWrapping = TextWrapping.Wrap;

            drawingCanvas.Children.Add(textBox);
            Canvas.SetLeft(textBox, graphicsText.Rectangle.Left);
            Canvas.SetTop(textBox, graphicsText.Rectangle.Top);
            textBox.Width = textBox.Width;
            textBox.Height = textBox.Height;

            textBox.Focus();
            textBox.LostFocus += new RoutedEventHandler(textBox_LostFocus);
            textBox.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(textBox_LostKeyboardFocus);
            textBox.PreviewKeyDown += new KeyEventHandler(textBox_PreviewKeyDown);
            textBox.ContextMenu = null;
            textBox.Loaded += new RoutedEventHandler(textBox_Loaded);
        }


        /// <summary>
        /// Определяет корректное местоположение текста
        /// </summary>
        void textBox_Loaded(object sender, RoutedEventArgs e)
        {
            double xOffset, yOffset;
            ComputeTextOffset(textBox, out xOffset, out yOffset);
            Canvas.SetLeft(textBox, Canvas.GetLeft(textBox) - xOffset);
            Canvas.SetTop(textBox, Canvas.GetTop(textBox) - yOffset);
            textBox.Width = textBox.Width + xOffset + xOffset;
            textBox.Height = textBox.Height + yOffset + yOffset;
        }


        /// <summary>
        /// Вычисляет расстояние между верхней левой точкок textbox и фактического текста
        /// </summary>
        static void ComputeTextOffset(TextBox tb, out double xOffset, out double yOffset)
        {
            xOffset = 5;
            yOffset = 3;
            try
            {
                ContentControl cc = (ContentControl)tb.Template.FindName("PART_ContentHost", tb);
                GeneralTransform tf = ((Visual)cc.Content).TransformToAncestor(tb);
                Point offset = tf.Transform(new Point(0, 0));
                xOffset = offset.X;
                yOffset = offset.Y;
            }
            catch (ArgumentException e)
            {
                System.Diagnostics.Trace.WriteLine("ComputeTextOffset: " + e.Message);
            }
            catch (InvalidOperationException e)
            {
                System.Diagnostics.Trace.WriteLine("ComputeTextOffset: " + e.Message);
            }
        }

        /// <summary>
        /// Скрывает textbox, когда Enter или Esc меняют состояние
        /// </summary>
        void textBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                textBox.Text = oldText;
                drawingCanvas.HideTextbox(editedGraphicsText);
                e.Handled = true;
                return;
            }

            if (e.Key == Key.Return && Keyboard.Modifiers == ModifierKeys.None)
            {
                drawingCanvas.HideTextbox(editedGraphicsText);
                e.Handled = true;
                return;
            }
        }

        /// <summary>
        /// Скрывает textbox, когда потерян фокус
        /// </summary>
        void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            drawingCanvas.HideTextbox(editedGraphicsText);
        }


        /// <summary>
        /// Скрывает textbox, когда потерян фокус клавиатуры
        /// </summary>
        void textBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            drawingCanvas.HideTextbox(editedGraphicsText);
        }

        /// <summary>
        /// Старый текст
        /// </summary>
        public string OldText
        {
            get { return oldText; }
        }
    }
}

