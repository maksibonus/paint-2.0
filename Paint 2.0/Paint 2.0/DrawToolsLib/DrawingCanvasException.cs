﻿using System;
using System.Runtime.Serialization;

namespace DrawToolsLib
{
    /// <summary>
    /// Исключение, выданное DrawingCanvas при загрузке и сохранении
    /// </summary>
    [Serializable]
    public class DrawingCanvasException : Exception
    {
        public DrawingCanvasException(string message)
            : base(message)
        { }

        public DrawingCanvasException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public DrawingCanvasException()
            : base(Properties.Settings.Default.UnknownException)
        { }

        protected DrawingCanvasException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
