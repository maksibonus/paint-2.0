﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Инструмент указателя
    /// </summary>
    class ToolPointer : Tool
    {
        private enum SelectionMode
        {
            None,
            Move,           // объект двигается
            Size,           // объект изменяет размер
            GroupSelection
        }

        private SelectionMode selectMode = SelectionMode.None;
        private GraphicsBase resizedObject;
        private int resizedObjectHandle;

        // Состояние текущей точки
        private Point lastPoint = new Point(0, 0);
        private CommandChangeState commandChangeState;
        bool wasMove;

        public ToolPointer()
        { }

        /// <summary>
        /// Обработчик нажатия мыши
        /// </summary>
        public override void OnMouseDown(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            commandChangeState = null;
            wasMove = false;
            Point point = e.GetPosition(drawingCanvas);
            selectMode = SelectionMode.None;
            GraphicsBase o;
            GraphicsBase movedObject = null;
            int handleNumber;

            // Проверка на изменение размера
            for (int i = drawingCanvas.GraphicsList.Count - 1; i >= 0; i--)
            {
                o = drawingCanvas[i];
                if (o.IsSelected)
                {
                    handleNumber = o.MakeHitTest(point);
                    if (handleNumber > 0)
                    {
                        selectMode = SelectionMode.Size;
                        resizedObject = o;
                        resizedObjectHandle = handleNumber;

                        // Отменяем выбор остальных объектов
                        HelperFunctions.UnselectAll(drawingCanvas);
                        o.IsSelected = true;
                        commandChangeState = new CommandChangeState(drawingCanvas);
                        break;
                    }
                }
            }

            // Проверка на движение
            if (selectMode == SelectionMode.None)
            {
                for (int i = drawingCanvas.GraphicsList.Count - 1; i >= 0; i--)
                {
                    o = drawingCanvas[i];
                    if (o.MakeHitTest(point) == 0)
                    {
                        movedObject = o;
                        break;
                    }
                }

                if (movedObject != null)
                {
                    selectMode = SelectionMode.Move;

                    // Отменяем выбор остальных объектов, если не нажат Ctrl 
                    // и объект не выбран
                    if (Keyboard.Modifiers != ModifierKeys.Control && !movedObject.IsSelected)
                    {
                        HelperFunctions.UnselectAll(drawingCanvas);
                    }

                    movedObject.IsSelected = true;
                    drawingCanvas.Cursor = Cursors.SizeAll;
                    commandChangeState = new CommandChangeState(drawingCanvas);
                }
            }

            // Нажатие на полотне
            if (selectMode == SelectionMode.None)
            {
                // Отменяем выбор остальных объектов, если не нажат Ctrl 
                if (Keyboard.Modifiers != ModifierKeys.Control)
                {
                    HelperFunctions.UnselectAll(drawingCanvas);
                }

                GraphicsSelectionRectangle r = new GraphicsSelectionRectangle(
                    point.X, point.Y,
                    point.X + 1, point.Y + 1,
                    drawingCanvas.ActualScale);
                r.Clip = new RectangleGeometry(new Rect(0, 0, drawingCanvas.ActualWidth, drawingCanvas.ActualHeight));
                drawingCanvas.GraphicsList.Add(r);
                selectMode = SelectionMode.GroupSelection;
            }
            lastPoint = point;
            drawingCanvas.CaptureMouse();
        }

        /// <summary>
        /// Обработка перемещения мыши
        /// </summary>
        public override void OnMouseMove(DrawingCanvas drawingCanvas, MouseEventArgs e)
        {
            // Исключает все случаи, кроме ЛКМ
            if (e.MiddleButton == MouseButtonState.Pressed ||
                 e.RightButton == MouseButtonState.Pressed)
            {
                drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
                return;
            }

            Point point = e.GetPosition(drawingCanvas);
            if (e.LeftButton == MouseButtonState.Released)
            {
                Cursor cursor = null;
                for (int i = 0; i < drawingCanvas.Count; i++)
                {
                    int n = drawingCanvas[i].MakeHitTest(point);
                    if (n > 0)
                    {
                        cursor = drawingCanvas[i].GetHandleCursor(n);
                        break;
                    }
                }

                if (cursor == null)
                    cursor = HelperFunctions.DefaultCursor;
                drawingCanvas.Cursor = cursor;
                return;
            }
            if (!drawingCanvas.IsMouseCaptured)
            {
                return;
            }
            wasMove = true;

            // Поиск разницы между новой и предыдущей позициями
            double dx = point.X - lastPoint.X;
            double dy = point.Y - lastPoint.Y;
            lastPoint = point;

            // Изменение размера
            if (selectMode == SelectionMode.Size)
            {
                if (resizedObject != null)
                {
                    resizedObject.MoveHandleTo(point, resizedObjectHandle);
                }
            }

            // Движение
            if (selectMode == SelectionMode.Move)
            {
                foreach (GraphicsBase o in drawingCanvas.Selection)
                {
                    o.Move(dx, dy);
                }
            }

            // Групповой выбор
            if (selectMode == SelectionMode.GroupSelection)
            {
                // Измение размеров выбранных объектов
                drawingCanvas[drawingCanvas.Count - 1].MoveHandleTo(
                    point, 5);
            }
        }

        /// <summary>
        /// Обработка отпускания кнопки мыши
        /// </summary>
        public override void OnMouseUp(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            if (!drawingCanvas.IsMouseCaptured)
            {
                drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
                selectMode = SelectionMode.None;
                return;
            }

            if (resizedObject != null)
            {
                // После изменения размеров
                resizedObject.Normalize();

                // Для текста
                if (resizedObject is GraphicsText)
                {
                    ((GraphicsText)resizedObject).RefreshDrawing();
                }
                resizedObject = null;
            }
            if (selectMode == SelectionMode.GroupSelection)
            {
                GraphicsSelectionRectangle r = (GraphicsSelectionRectangle)drawingCanvas[drawingCanvas.Count - 1];
                r.Normalize();
                Rect rect = r.Rectangle;
                drawingCanvas.GraphicsList.Remove(r);
                foreach (GraphicsBase g in drawingCanvas.GraphicsList)
                {
                    if (g.IntersectsWith(rect))
                    {
                        g.IsSelected = true;
                    }
                }
            }
            drawingCanvas.ReleaseMouseCapture();
            drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
            selectMode = SelectionMode.None;
            AddChangeToHistory(drawingCanvas);
        }

        /// <summary>
        /// Установить курсор
        /// </summary>
        public override void SetCursor(DrawingCanvas drawingCanvas)
        {
            drawingCanvas.Cursor = HelperFunctions.DefaultCursor;
        }

        /// <summary>
        /// Добавить изменение в историю
        /// </summary>
        public void AddChangeToHistory(DrawingCanvas drawingCanvas)
        {
            if (commandChangeState != null && wasMove)
            {
                commandChangeState.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(commandChangeState);
                commandChangeState = null;
            }
        }
    }
}

