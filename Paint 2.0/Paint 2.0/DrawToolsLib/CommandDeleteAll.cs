﻿using System.Collections.Generic;

namespace DrawToolsLib
{
    /// <summary>
    /// Команда удалить все
    /// </summary>
    class CommandDeleteAll : CommandBase
    {
        List<PropertiesGraphicsBase> cloneList;

        public CommandDeleteAll(DrawingCanvas drawingCanvas)
        {
            cloneList = new List<PropertiesGraphicsBase>();
            foreach (GraphicsBase g in drawingCanvas.GraphicsList)
            {
                cloneList.Add(g.CreateSerializedObject());
            }
        }

        /// <summary>
        /// Добавляем все удаленные объекты в лист
        /// </summary>
        public override void Undo(DrawingCanvas drawingCanvas)
        {
            foreach (PropertiesGraphicsBase o in cloneList)
            {
                drawingCanvas.GraphicsList.Add(o.CreateGraphics());
            }
            drawingCanvas.RefreshClip();
        }

        /// <summary>
        /// Удаляем все снова
        /// </summary>
        public override void Redo(DrawingCanvas drawingCanvas)
        {
            drawingCanvas.GraphicsList.Clear();
        }
    }
}

