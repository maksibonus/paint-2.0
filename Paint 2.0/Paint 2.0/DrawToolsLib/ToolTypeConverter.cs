﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DrawToolsLib
{
    /// <summary>
    /// Конвертирувет ToolType к bool.
    /// ToolType должен быть "Pointer", "Rectangle" и другое
    /// </summary>
    [ValueConversion(typeof(ToolType), typeof(bool))]
    public class ToolTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            string name = Enum.GetName(typeof(ToolType), value);
            return (name == (string)parameter);
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return new NotSupportedException(this.GetType().Name + Properties.Settings.Default.ConvertBackNotSupported);
        }
    }
}

