﻿namespace DrawToolsLib
{
    /// <summary>
    /// Базовый класс для команды, используемые для Отмена - Повторить
    /// </summary>
    abstract class CommandBase
    {
        // Функция используется для того, чтобы отменить операцию
        public abstract void Undo(DrawingCanvas drawingCanvas);

        // Функция используется для того, чтобы повторить операцию
        public abstract void Redo(DrawingCanvas drawingCanvas);
    }
}
