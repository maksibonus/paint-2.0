﻿using System.IO;
using System.Windows;
using System.Windows.Input;


namespace DrawToolsLib
{
    /// <summary>
    /// Инструмент линии
    /// </summary>
    class ToolLine : ToolObject
    {
        public ToolLine()
        {
            MemoryStream stream = new MemoryStream(Properties.Resources.Line);
            ToolCursor = new Cursor(stream);
        }

        /// <summary>
        /// Создат новый объект
        /// </summary>
        public override void OnMouseDown(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(drawingCanvas);
            AddNewObject(drawingCanvas,
                new GraphicsLine(
                p,
                new Point(p.X + 1, p.Y + 1),
                drawingCanvas.LineWidth,
                drawingCanvas.ObjectColor,
                drawingCanvas.ActualScale));
        }

        /// <summary>
        /// Устанавливает курсор и изменяет размер нового объекта
        /// </summary>
        public override void OnMouseMove(DrawingCanvas drawingCanvas, MouseEventArgs e)
        {
            drawingCanvas.Cursor = ToolCursor;
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                drawingCanvas[drawingCanvas.Count - 1].MoveHandleTo(
                    e.GetPosition(drawingCanvas), 2);
            }
        }
    }
}

