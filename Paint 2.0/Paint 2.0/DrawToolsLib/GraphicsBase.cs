﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;


namespace DrawToolsLib
{
    /// <summary>
    /// Базовый класс для графических объектов
    /// </summary>
    public abstract class GraphicsBase : DrawingVisual
    {
        #region Члены
        // Позволяет писать Undo - Redo функции и не волноваться об объектах, порядок в списке
        int objectId;

        // Объекты для рисования
        // Внешний прямоугольник
        static SolidColorBrush handleBrush1 = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        // Средний прямоугольник
        static SolidColorBrush handleBrush2 = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        // Внутренний прямоугольник
        static SolidColorBrush handleBrush3 = new SolidColorBrush(Color.FromArgb(255, 0, 0, 255));

        protected double graphicsLineWidth;
        protected Color graphicsObjectColor;
        protected double graphicsActualScale;
        protected bool selected;
        protected const double HitTestWidth = 8.0;
        protected const double HandleSize = 12.0;

        #endregion Члены

        #region Конструктор
        protected GraphicsBase()
        {
            objectId = this.GetHashCode();
        }

        #endregion Конструктор

        #region Свойства
        public bool IsSelected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                RefreshDrawing();
            }
        }

        public double LineWidth
        {
            get
            {
                return graphicsLineWidth;
            }

            set
            {
                graphicsLineWidth = value;
                RefreshDrawing();
            }
        }

        public Color ObjectColor
        {
            get
            {
                return graphicsObjectColor;
            }

            set
            {
                graphicsObjectColor = value;
                RefreshDrawing();
            }
        }

        public double ActualScale
        {
            get
            {
                return graphicsActualScale;
            }

            set
            {
                graphicsActualScale = value;
                RefreshDrawing();
            }
        }

        protected double ActualLineWidth
        {
            get
            {
                return graphicsActualScale <= 0 ? graphicsLineWidth : graphicsLineWidth / graphicsActualScale;
            }
        }

        protected double LineHitTestWidth
        {
            get
            {
                return Math.Max(8.0, ActualLineWidth);
            }
        }

        public int Id
        {
            get { return objectId; }
            set { objectId = value; }
        }

        #endregion Свойства

        #region Абстрактные методы и свойства
        /// <summary>
        /// Возвращает количество обработчиков
        /// </summary>
        public abstract int HandleCount
        {
            get;
        }

        /// <summary>
        /// Проверить содержит ли объект точку
        /// </summary>
        public abstract bool Contains(Point point);

        /// <summary>
        /// Создает объект для сериализации
        /// </summary>
        public abstract PropertiesGraphicsBase CreateSerializedObject();

        /// <summary>
        /// Получить первую точку обработчика
        /// </summary>
        public abstract Point GetHandle(int handleNumber);

        /// <summary>
        /// Проверка нажатия 
        /// Возвращаемое значения: -1 - нет нажатия
        ///                         0 - нажатие где-то
        ///                         > 0 - количество обработчиков
        /// </summary>
        public abstract int MakeHitTest(Point point);


        /// <summary>
        /// Проверить, пересекает ли объект  прямоугольник
        /// </summary>
        public abstract bool IntersectsWith(Rect rectangle);

        /// <summary>
        /// Двигать объект
        /// </summary>
        public abstract void Move(double deltaX, double deltaY);


        /// <summary>
        /// Переместить обрабочик к точке
        /// </summary>
        public abstract void MoveHandleTo(Point point, int handleNumber);

        /// <summary>
        /// Получить обработчик курсора
        /// </summary>
        public abstract Cursor GetHandleCursor(int handleNumber);

        #endregion Абстрактные методы и свойства

        #region Виртуальные методы
        /// <summary>
        /// Нормализирует объект.
        /// Вызывается после изменение размеров
        /// </summary>
        public virtual void Normalize()
        {
            // Для линии не надо
        }

        /// <summary>
        /// Реализует фактический код для прорисовки
        /// </summary>
        public virtual void Draw(DrawingContext drawingContext)
        {
            if (IsSelected)
            {
                DrawTracker(drawingContext);
            }
        }

        /// <summary>
        /// Рисовать отслеживание для выбранного объекта
        /// </summary>
        public virtual void DrawTracker(DrawingContext drawingContext)
        {
            for (int i = 1; i <= HandleCount; i++)
            {
                DrawTrackerRectangle(drawingContext, GetHandleRectangle(i));
            }
        }

        #endregion Виртуальные методы

        #region Другие методы
        /// <summary>
        /// Рисовать отслеживаемый прямоугольник
        /// </summary>
        static void DrawTrackerRectangle(DrawingContext drawingContext, Rect rectangle)
        {
            // Внешний прямоугольник
            drawingContext.DrawRectangle(handleBrush1, null, rectangle);
            // Средний прямоугольник
            drawingContext.DrawRectangle(handleBrush2, null,
                new Rect(rectangle.Left + rectangle.Width / 8,
                         rectangle.Top + rectangle.Height / 8,
                         rectangle.Width * 6 / 8,
                         rectangle.Height * 6 / 8));
            // Внутренний прямоугольник
            drawingContext.DrawRectangle(handleBrush3, null,
                new Rect(rectangle.Left + rectangle.Width / 4,
                 rectangle.Top + rectangle.Height / 4,
                 rectangle.Width / 2,
                 rectangle.Height / 2));
        }

        /// <summary>
        /// Обновляет прорисовку, после изменения свойства
        /// </summary>
        public void RefreshDrawing()
        {
            DrawingContext dc = this.RenderOpen();
            Draw(dc);
            dc.Close();
        }

        /// <summary>
        /// Возвращает обработанный прямоугольник
        /// </summary>
        public Rect GetHandleRectangle(int handleNumber)
        {
            Point point = GetHandle(handleNumber);
            double size = Math.Max(HandleSize / graphicsActualScale, ActualLineWidth * 1.1);
            return new Rect(point.X - size / 2, point.Y - size / 2,
                size, size);
        }

        #endregion Другие методы
    }
}
