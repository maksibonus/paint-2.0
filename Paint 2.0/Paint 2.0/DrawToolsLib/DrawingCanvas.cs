﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace DrawToolsLib
{
    /// <summary>
    /// Полотно, которое принимает объекты находящиеся на нем.
    /// Позволяет рисовать графические объекты с помощью мыши
    /// </summary>
    public class DrawingCanvas : Canvas
    {
        #region Члены
        // Коллекция содержит экземпляры производных классов GraphicsBase
        private VisualCollection graphicsList;

        private Tool[] tools;
        private ToolText toolText;
        private ToolPointer toolPointer;
        private ContextMenu contextMenu;
        private UndoManager undoManager;

        // Зависимые свойства
        public static readonly DependencyProperty ToolProperty;
        public static readonly DependencyProperty ActualScaleProperty;
        public static readonly DependencyProperty IsDirtyProperty;

        public static readonly DependencyProperty LineWidthProperty;
        public static readonly DependencyProperty ObjectColorProperty;

        public static readonly DependencyProperty TextFontFamilyNameProperty;
        public static readonly DependencyProperty TextFontStyleProperty;
        public static readonly DependencyProperty TextFontWeightProperty;
        public static readonly DependencyProperty TextFontStretchProperty;
        public static readonly DependencyProperty TextFontSizeProperty;

        public static readonly DependencyProperty CanUndoProperty;
        public static readonly DependencyProperty CanRedoProperty;

        public static readonly DependencyProperty CanSelectAllProperty;
        public static readonly DependencyProperty CanUnselectAllProperty;
        public static readonly DependencyProperty CanDeleteProperty;
        public static readonly DependencyProperty CanDeleteAllProperty;
        public static readonly DependencyProperty CanMoveToFrontProperty;
        public static readonly DependencyProperty CanMoveToBackProperty;
        public static readonly DependencyProperty CanSetPropertiesProperty;

        public static readonly RoutedEvent IsDirtyChangedEvent;

        #endregion Члены

        #region Конструкторы
        public DrawingCanvas()
            : base()
        {
            graphicsList = new VisualCollection(this);
            CreateContextMenu();

            // Создаем массив инструментов рисования
            tools = new Tool[(int)ToolType.Max];
            toolPointer = new ToolPointer();
            tools[(int)ToolType.Pointer] = toolPointer;
            tools[(int)ToolType.Rectangle] = new ToolRectangle();
            tools[(int)ToolType.Ellipse] = new ToolEllipse();
            tools[(int)ToolType.Line] = new ToolLine();
            tools[(int)ToolType.PolyLine] = new ToolPolyLine();
            toolText = new ToolText(this);
            tools[(int)ToolType.Text] = toolText;

            // Создаем UndoManager
            undoManager = new UndoManager(this);
            undoManager.StateChanged += new EventHandler(undoManager_StateChanged);

            this.FocusVisualStyle = null;

            //Задаем обработчики событий
            this.Loaded += new RoutedEventHandler(DrawingCanvas_Loaded);
            this.MouseDown += new MouseButtonEventHandler(DrawingCanvas_MouseDown);
            this.MouseMove += new MouseEventHandler(DrawingCanvas_MouseMove);
            this.MouseUp += new MouseButtonEventHandler(DrawingCanvas_MouseUp);
            this.KeyDown += new KeyEventHandler(DrawingCanvas_KeyDown);
            this.LostMouseCapture += new MouseEventHandler(DrawingCanvas_LostMouseCapture);
        }


        static DrawingCanvas()
        {
            // **********************************************************
            // Создаем зависимые свойства
            // **********************************************************

            PropertyMetadata metaData;

            // Tool
            metaData = new PropertyMetadata(ToolType.Pointer);
            ToolProperty = DependencyProperty.Register(
                "Tool", typeof(ToolType), typeof(DrawingCanvas),
                metaData);

            // ActualScale
            metaData = new PropertyMetadata(
                1.0,                                                        // значение по умолчанию
                new PropertyChangedCallback(ActualScaleChanged));           // изменяем обратный вызов
            ActualScaleProperty = DependencyProperty.Register(
                "ActualScale", typeof(double), typeof(DrawingCanvas),
                metaData);

            // IsDirty
            metaData = new PropertyMetadata(false);
            IsDirtyProperty = DependencyProperty.Register(
                "IsDirty", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // LineWidth
            metaData = new PropertyMetadata(
                1.0,
                new PropertyChangedCallback(LineWidthChanged));
            LineWidthProperty = DependencyProperty.Register(
                "LineWidth", typeof(double), typeof(DrawingCanvas),
                metaData);

            // ObjectColor
            metaData = new PropertyMetadata(
                Colors.Black,
                new PropertyChangedCallback(ObjectColorChanged));
            ObjectColorProperty = DependencyProperty.Register(
                "ObjectColor", typeof(Color), typeof(DrawingCanvas),
                metaData);


            // TextFontFamilyName
            metaData = new PropertyMetadata(
                Properties.Settings.Default.DefaultFontFamily,
                new PropertyChangedCallback(TextFontFamilyNameChanged));
            TextFontFamilyNameProperty = DependencyProperty.Register(
                "TextFontFamilyName", typeof(string), typeof(DrawingCanvas),
                metaData);

            // TextFontStyle
            metaData = new PropertyMetadata(
                FontStyles.Normal,
                new PropertyChangedCallback(TextFontStyleChanged));
            TextFontStyleProperty = DependencyProperty.Register(
                "TextFontStyle", typeof(FontStyle), typeof(DrawingCanvas),
                metaData);

            // TextFontWeight
            metaData = new PropertyMetadata(
                FontWeights.Normal,
                new PropertyChangedCallback(TextFontWeightChanged));
            TextFontWeightProperty = DependencyProperty.Register(
                "TextFontWeight", typeof(FontWeight), typeof(DrawingCanvas),
                metaData);

            // TextFontStretch
            metaData = new PropertyMetadata(
                FontStretches.Normal,
                new PropertyChangedCallback(TextFontStretchChanged));
            TextFontStretchProperty = DependencyProperty.Register(
                "TextFontStretch", typeof(FontStretch), typeof(DrawingCanvas),
                metaData);

            // TextFontSize
            metaData = new PropertyMetadata(
                12.0,
                new PropertyChangedCallback(TextFontSizeChanged));
            TextFontSizeProperty = DependencyProperty.Register(
                "TextFontSize", typeof(double), typeof(DrawingCanvas),
                metaData);

            // CanUndo
            metaData = new PropertyMetadata(false);
            CanUndoProperty = DependencyProperty.Register(
                "CanUndo", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanRedo
            metaData = new PropertyMetadata(false);
            CanRedoProperty = DependencyProperty.Register(
                "CanRedo", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanSelectAll
            metaData = new PropertyMetadata(false);
            CanSelectAllProperty = DependencyProperty.Register(
                "CanSelectAll", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanUnselectAll
            metaData = new PropertyMetadata(false);
            CanUnselectAllProperty = DependencyProperty.Register(
                "CanUnselectAll", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanDelete
            metaData = new PropertyMetadata(false);
            CanDeleteProperty = DependencyProperty.Register(
                "CanDelete", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanDeleteAll
            metaData = new PropertyMetadata(false);
            CanDeleteAllProperty = DependencyProperty.Register(
                "CanDeleteAll", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanMoveToFront
            metaData = new PropertyMetadata(false);
            CanMoveToFrontProperty = DependencyProperty.Register(
                "CanMoveToFront", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanMoveToBack
            metaData = new PropertyMetadata(false);
            CanMoveToBackProperty = DependencyProperty.Register(
                "CanMoveToBack", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // CanSetProperties
            metaData = new PropertyMetadata(false);
            CanSetPropertiesProperty = DependencyProperty.Register(
                "CanSetProperties", typeof(bool), typeof(DrawingCanvas),
                metaData);

            // **********************************************************
            // Создание маршрутизированного события
            // **********************************************************

            // IsDirtyChanged
            IsDirtyChangedEvent = EventManager.RegisterRoutedEvent("IsDirtyChangedChanged",
                RoutingStrategy.Bubble, typeof(DependencyPropertyChangedEventHandler), typeof(DrawingCanvas));

        }

        #endregion Конструкторы

        #region Зависимые свойства

        #region Tool
        /// <summary>
        /// Текущий инструмент
        /// </summary>
        public ToolType Tool
        {
            get
            {
                return (ToolType)GetValue(ToolProperty);
            }
            set
            {
                if ((int)value >= 0 && (int)value < (int)ToolType.Max)
                {
                    SetValue(ToolProperty, value);
                    // Установливает курсор сразу - важно, когда инструмент выбран из меню
                    tools[(int)Tool].SetCursor(this);
                }
            }
        }

        #endregion Tool

        #region ActualScale
        /// <summary>
        /// Масштабирует размер
        /// </summary>
        public double ActualScale
        {
            get
            {
                return (double)GetValue(ActualScaleProperty);
            }
            set
            {
                SetValue(ActualScaleProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова, вызываемая, когда свойство ActualScale изменяется
        /// </summary>
        static void ActualScaleChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            double scale = d.ActualScale;
            foreach (GraphicsBase b in d.GraphicsList)
            {
                b.ActualScale = scale;
            }
        }

        #endregion ActualScale

        #region IsDirty
        /// <summary>
        /// Является ли документ измененным
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return (bool)GetValue(IsDirtyProperty);
            }
            internal set
            {
                SetValue(IsDirtyProperty, value);
                // Вызываем событие IsDirtyChanged
                RoutedEventArgs newargs = new RoutedEventArgs(IsDirtyChangedEvent);
                RaiseEvent(newargs);
            }
        }

        #endregion IsDirty

        #region CanUndo
        /// <summary>
        /// Можно ли использовать операцию Отменить
        /// </summary>
        public bool CanUndo
        {
            get
            {
                return (bool)GetValue(CanUndoProperty);
            }
            internal set
            {
                SetValue(CanUndoProperty, value);
            }
        }

        #endregion CanUndo

        #region CanRedo
        /// <summary>
        /// Можно ли использовать операцию Повторить
        /// </summary>
        public bool CanRedo
        {
            get
            {
                return (bool)GetValue(CanRedoProperty);
            }
            internal set
            {
                SetValue(CanRedoProperty, value);
            }
        }

        #endregion CanRedo

        #region CanSelectAll
        /// <summary>
        /// Доступны ли все выбранные функции
        /// </summary>
        public bool CanSelectAll
        {
            get
            {
                return (bool)GetValue(CanSelectAllProperty);
            }
            internal set
            {
                SetValue(CanSelectAllProperty, value);
            }
        }

        #endregion CanSelectAll

        #region CanUnselectAll
        /// <summary>
        /// Доступны ли все не выбранные функции
        /// </summary>
        public bool CanUnselectAll
        {
            get
            {
                return (bool)GetValue(CanUnselectAllProperty);
            }
            internal set
            {
                SetValue(CanUnselectAllProperty, value);
            }
        }

        #endregion CanUnselectAll

        #region CanDelete
        /// <summary>
        /// Доступна ли функция удаления
        /// </summary>
        public bool CanDelete
        {
            get
            {
                return (bool)GetValue(CanDeleteProperty);
            }
            internal set
            {
                SetValue(CanDeleteProperty, value);
            }
        }

        #endregion CanDelete

        #region CanDeleteAll
        /// <summary>
        /// Доступна ли функция удаления всего
        /// </summary>
        public bool CanDeleteAll
        {
            get
            {
                return (bool)GetValue(CanDeleteAllProperty);
            }
            internal set
            {
                SetValue(CanDeleteAllProperty, value);
            }
        }

        #endregion CanDeleteAll

        #region CanMoveToFront
        /// <summary>
        /// Доступна ли функция перевода объекта на передний план
        /// </summary>
        public bool CanMoveToFront
        {
            get
            {
                return (bool)GetValue(CanMoveToFrontProperty);
            }
            internal set
            {
                SetValue(CanMoveToFrontProperty, value);
            }
        }

        #endregion CanMoveToFront

        #region CanMoveToBack
        /// <summary>
        ///  Доступна ли функция перевода объекта на задний план
        /// </summary>
        public bool CanMoveToBack
        {
            get
            {
                return (bool)GetValue(CanMoveToBackProperty);
            }
            internal set
            {
                SetValue(CanMoveToBackProperty, value);
            }
        }

        #endregion CanMoveToBack

        #region CanSetProperties
        /// <summary>
        /// Можно ли применить активные свойства(размер шрифта, цвет и т.д.) для выбранного объекта
        /// </summary>
        public bool CanSetProperties
        {
            get
            {
                return (bool)GetValue(CanSetPropertiesProperty);
            }
            internal set
            {
                SetValue(CanSetPropertiesProperty, value);
            }
        }

        #endregion CanSetProperties

        #region LineWidth
        /// <summary>
        /// Ширина линии новому или выбранному графического объекта
        /// </summary>
        public double LineWidth
        {
            get
            {
                return (double)GetValue(LineWidthProperty);
            }
            set
            {
                SetValue(LineWidthProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство LineWidth изменяется
        /// </summary>
        static void LineWidthChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyLineWidth(d, d.LineWidth, true);
        }

        #endregion LineWidth

        #region ObjectColor
        /// <summary>
        /// Цвет нового или выбранного графического объекта 
        /// </summary>
        public Color ObjectColor
        {
            get
            {
                return (Color)GetValue(ObjectColorProperty);
            }
            set
            {
                SetValue(ObjectColorProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство ObjectColor изменяется
        /// </summary>
        static void ObjectColorChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyColor(d, d.ObjectColor, true);
        }

        #endregion ObjectColor

        #region TextFontFamilyName
        /// <summary>
        /// Имя семейства шрифтов нового или выбранного графического объекта 	
        /// </summary>
        public string TextFontFamilyName
        {
            get
            {
                return (string)GetValue(TextFontFamilyNameProperty);
            }
            set
            {
                SetValue(TextFontFamilyNameProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство TextFontFamilyName изменяется
        /// </summary>
        static void TextFontFamilyNameChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyFontFamily(d, d.TextFontFamilyName, true);
        }

        #endregion TextFontFamilyName

        #region TextFontStyle
        /// <summary>
        /// Стиль шрифта нового или выбранного графического объекта 	
        /// </summary>
        public FontStyle TextFontStyle
        {
            get
            {
                return (FontStyle)GetValue(TextFontStyleProperty);
            }
            set
            {
                SetValue(TextFontStyleProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство TextFontStyle изменяется
        /// </summary>
        static void TextFontStyleChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyFontStyle(d, d.TextFontStyle, true);
        }

        #endregion TextFontStyle

        #region TextFontWeight
        /// <summary>
        /// Насыщенность шрифта нового или выбранного графического объекта 	
        /// </summary>
        public FontWeight TextFontWeight
        {
            get
            {
                return (FontWeight)GetValue(TextFontWeightProperty);
            }
            set
            {
                SetValue(TextFontWeightProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство TextFontWeight изменяется
        /// </summary>
        static void TextFontWeightChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyFontWeight(d, d.TextFontWeight, true);
        }

        #endregion TextFontWeight

        #region TextFontStretch
        /// <summary>
        /// Ширина шрифта нового или выбранного графического объекта 	
        /// </summary>
        public FontStretch TextFontStretch
        {
            get
            {
                return (FontStretch)GetValue(TextFontStretchProperty);
            }
            set
            {
                SetValue(TextFontStretchProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство TextFontStretch изменяется
        /// </summary>
        static void TextFontStretchChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyFontStretch(d, d.TextFontStretch, true);
        }

        #endregion TextFontStretch

        #region TextFontSize
        /// <summary>
        /// Размер шрифта нового или выбранного графического объекта 	
        /// </summary>
        public double TextFontSize
        {
            get
            {
                return (double)GetValue(TextFontSizeProperty);
            }
            set
            {
                SetValue(TextFontSizeProperty, value);
            }
        }

        /// <summary>
        /// Функция обратного вызова вызывается, когда свойство TextFontSize изменяется
        /// </summary>
        static void TextFontSizeChanged(DependencyObject property, DependencyPropertyChangedEventArgs args)
        {
            DrawingCanvas d = property as DrawingCanvas;
            HelperFunctions.ApplyFontSize(d, d.TextFontSize, true);
        }

        #endregion TextFontSize

        #endregion Зависимые свойства

        #region Маршрутизированные события
        /// <summary>
        /// Событие несохраненных данных
        /// </summary>
        public event RoutedEventHandler IsDirtyChanged
        {
            add { AddHandler(IsDirtyChangedEvent, value); }
            remove { RemoveHandler(IsDirtyChangedEvent, value); }
        }

        #endregion Маршрутизированные события

        #region Открытые функции
        /// <summary>
        /// Везвращает список графических объектов
        /// </summary>
        public PropertiesGraphicsBase[] GetListOfGraphicObjects()
        {
            PropertiesGraphicsBase[] result = new PropertiesGraphicsBase[graphicsList.Count];

            int i = 0;

            foreach (GraphicsBase g in graphicsList)
            {
                result[i++] = g.CreateSerializedObject();
            }

            return result;
        }

        /// <summary>
        /// Рисует все графические объекты DrawingContext, созданные клиентом.
        /// </summary>
        public void Draw(DrawingContext drawingContext)
        {
            Draw(drawingContext, false);
        }

        /// <summary>
        /// Рисует все графические объекты DrawingContext, созданные клиентом.
        /// withSelection = true - рисовать выбранные объекты с отслеживанием
        /// </summary>
        public void Draw(DrawingContext drawingContext, bool withSelection)
        {
            bool oldSelection = false;
            foreach (GraphicsBase b in graphicsList)
            {
                if (!withSelection)
                {
                    oldSelection = b.IsSelected;
                    b.IsSelected = false;
                }

                b.Draw(drawingContext);

                if (!withSelection)
                {
                    // Возвращаем выбранное состояние
                    b.IsSelected = oldSelection;
                }
            }
        }

        /// <summary>
        /// Очищает графический лист
        /// </summary>
        public void Clear()
        {
            graphicsList.Clear();
            ClearHistory();
            UpdateState();
        }

        /// <summary>
        /// Сохраняет объекты в XML
        /// </summary>
        public void Save(string fileName)
        {
            try
            {
                SerializationHelper helper = new SerializationHelper(graphicsList);
                XmlSerializer xml = new XmlSerializer(typeof(SerializationHelper));
                using (Stream stream = new FileStream(fileName,
                    FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    xml.Serialize(stream, helper);
                    ClearHistory();
                    UpdateState();
                }
            }
            catch (IOException e)
            {
                throw new DrawingCanvasException(e.Message, e);
            }
            catch (InvalidOperationException e)
            {
                throw new DrawingCanvasException(e.Message, e);
            }
        }

        /// <summary>
        /// Загружает объекты из XML
        /// </summary>
        public void Load(string fileName)
        {
            try
            {
                SerializationHelper helper;
                XmlSerializer xml = new XmlSerializer(typeof(SerializationHelper));
                using (Stream stream = new FileStream(fileName,
                    FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    helper = (SerializationHelper)xml.Deserialize(stream);
                }
                if (helper.Graphics == null)
                {
                    throw new DrawingCanvasException(Properties.Settings.Default.NoInfoInXMLFile);
                }
                graphicsList.Clear();
                foreach (PropertiesGraphicsBase g in helper.Graphics)
                {
                    graphicsList.Add(g.CreateGraphics());
                }
                RefreshClip();
                ClearHistory();
                UpdateState();
            }
            catch (IOException e)
            {
                throw new DrawingCanvasException(e.Message, e);
            }
            catch (InvalidOperationException e)
            {
                throw new DrawingCanvasException(e.Message, e);
            }
            catch (ArgumentNullException e)
            {
                throw new DrawingCanvasException(e.Message, e);
            }
        }


        /// <summary>
        /// Выделить все объекты
        /// </summary>
        public void SelectAll()
        {
            HelperFunctions.SelectAll(this);
            UpdateState();
        }

        /// <summary>
        /// Отменить выделение всех объектов
        /// </summary>
        public void UnselectAll()
        {
            HelperFunctions.UnselectAll(this);
            UpdateState();
        }

        /// <summary>
        /// Удалить выбранный
        /// </summary>
        public void Delete()
        {
            HelperFunctions.DeleteSelection(this);
            UpdateState();
        }

        /// <summary>
        /// Удалить все
        /// </summary>
        public void DeleteAll()
        {
            HelperFunctions.DeleteAll(this);
            UpdateState();
        }

        /// <summary>
        /// Переместить выбранные объекты на передний план
        /// </summary>
        public void MoveToFront()
        {
            HelperFunctions.MoveSelectionToFront(this);
            UpdateState();
        }

        /// <summary>
        /// Переместить выбранные объекты на задний план
        /// </summary>
        public void MoveToBack()
        {
            HelperFunctions.MoveSelectionToBack(this);
            UpdateState();
        }

        /// <summary>
        /// Применяет активные свойства для выбранных объектов
        /// </summary>
        public void SetProperties()
        {
            HelperFunctions.ApplyProperties(this);
            UpdateState();
        }

        /// <summary>
        /// Устанавливает clip для всех графических объектов
        /// </summary>
        public void RefreshClip()
        {
            foreach (GraphicsBase b in graphicsList)
            {
                b.Clip = new RectangleGeometry(new Rect(0, 0, this.ActualWidth, this.ActualHeight));

                // Good chance to refresh actual scale
                b.ActualScale = this.ActualScale;
            }
        }

        /// <summary>
        /// Удаляет clip для всех графических объектов
        /// </summary>
        public void RemoveClip()
        {
            foreach (GraphicsBase b in graphicsList)
            {
                b.Clip = null;
            }
        }

        /// <summary>
        /// Отменить
        /// </summary>
        public void Undo()
        {
            undoManager.Undo();
            UpdateState();
        }

        /// <summary>
        /// Повторить
        /// </summary>
        public void Redo()
        {
            undoManager.Redo();
            UpdateState();
        }

        #endregion Открытые функции

        #region Внутренние свойства и индексаторы
        /// <summary>
        /// Получить объект по индексу
        /// </summary>
        internal GraphicsBase this[int index]
        {
            get
            {
                if (index >= 0 && index < Count)
                {
                    return (GraphicsBase)graphicsList[index];
                }
                return null;
            }
        }

        /// <summary>
        /// Получить количество объектов
        /// </summary>
        internal int Count
        {
            get
            {
                return graphicsList.Count;
            }
        }

        /// <summary>
        /// Получить количество выбранных объектов
        /// </summary>
        internal int SelectionCount
        {
            get
            {
                int count = 0;

                foreach (GraphicsBase g in this.graphicsList)
                {
                    if (g.IsSelected)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        /// <summary>
        /// Возвращает лист объектов
        /// </summary>
        internal VisualCollection GraphicsList
        {
            get
            {
                return graphicsList;
            }
        }

        /// <summary>
        /// Возвращает IEnumerable для перебора объектов
        /// </summary>
        internal IEnumerable<GraphicsBase> Selection
        {
            get
            {
                foreach (GraphicsBase o in graphicsList)
                {
                    if (o.IsSelected)
                    {
                        yield return o;
                    }
                }
            }
        }

        #endregion Внутренние свойства

        #region Переопределение методов визуальных объектов

        /// <summary>
        /// Возвращает количество детей. Если на месте редактирования textbox активен, добавляем 1.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get
            {
                int n = graphicsList.Count;

                if (toolText.TextBox != null)
                {
                    n++;
                }
                return n;
            }
        }

        /// <summary>
        /// Возвращает ребенка графического объекта
        /// </summary>
        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= graphicsList.Count)
            {
                if (toolText.TextBox != null && index == graphicsList.Count)
                {
                    return toolText.TextBox;
                }

                throw new ArgumentOutOfRangeException("index");
            }

            return graphicsList[index];
        }

        #endregion Переопределение методов визуальных объектов

        #region Обработчики событий мыши

        /// <summary>
        /// Мышь нажата.
        /// ЛКМ - передается активному инструменту.
        /// ПКМ - передается в этот же класс
        /// </summary>
        void DrawingCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (tools[(int)Tool] == null)
            {
                return;
            }
            this.Focus();
            if (e.ChangedButton == MouseButton.Left)
            {
                if (e.ClickCount == 2)
                {
                    HandleDoubleClick(e);        // для текста
                }
                else
                {
                    tools[(int)Tool].OnMouseDown(this, e);
                }
                UpdateState();
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                ShowContextMenu(e);
            }
        }

        /// <summary>
        /// Перемещение мыши
        /// Перемещение без нажатой кнопки или нажатой ЛКМ передается активному инструмента.
        /// </summary>
        void DrawingCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (tools[(int)Tool] == null)
            {
                return;
            }
            if (e.MiddleButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released)
            {
                tools[(int)Tool].OnMouseMove(this, e);
                UpdateState();
            }
            else
            {
                this.Cursor = HelperFunctions.DefaultCursor;
            }
        }

        /// <summary>
        /// Мышь отпущена
        /// ЛКМ - событие передается обработчику
        /// </summary>
        void DrawingCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (tools[(int)Tool] == null)
            {
                return;
            }
            if (e.ChangedButton == MouseButton.Left)
            {
                tools[(int)Tool].OnMouseUp(this, e);
                UpdateState();
            }
        }

        #endregion Обработчики событий мыши

        #region Другие обработчики событий

        /// <summary>
        /// Инициализация после загрузки контрола
        /// </summary>
        void DrawingCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // для обрабатки сообщений от клавиатуры
            this.Focusable = true;
        }

        /// <summary>
        /// Нажат пункт контекстного меню
        /// </summary>
        void contextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;

            if (item == null)
            {
                return;
            }
            ContextMenuCommand command = (ContextMenuCommand)item.Tag;
            switch (command)
            {
                case ContextMenuCommand.SelectAll:
                    SelectAll(); break;
                case ContextMenuCommand.UnselectAll:
                    UnselectAll(); break;
                case ContextMenuCommand.Delete:
                    Delete(); break;
                case ContextMenuCommand.DeleteAll:
                    DeleteAll(); break;
                case ContextMenuCommand.MoveToFront:
                    MoveToFront(); break;
                case ContextMenuCommand.MoveToBack:
                    MoveToBack(); break;
                case ContextMenuCommand.Undo:
                    Undo(); break;
                case ContextMenuCommand.Redo:
                    Redo(); break;
                case ContextMenuCommand.SetProperties:
                    SetProperties(); break;
            }
        }

        /// <summary>
        /// Захват мыши потерян
        /// </summary>
        void DrawingCanvas_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                CancelCurrentOperation();
                UpdateState();
            }
        }

        /// <summary>
        /// Обработка ввода с клавиатуры
        /// </summary>
        void DrawingCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            // Esc останавливает текущие операции
            if (e.Key == Key.Escape)
            {
                if (this.IsMouseCaptured)
                {
                    CancelCurrentOperation();
                    UpdateState();
                }
            }
        }

        /// <summary>
        /// Изменилось состояние UndoManager.
        /// Обновления свойств CanUndo, CanRedo и IsDirty
        /// </summary>
        void undoManager_StateChanged(object sender, EventArgs e)
        {
            this.CanUndo = undoManager.CanUndo;
            this.CanRedo = undoManager.CanRedo;
            if (undoManager.IsDirty != this.IsDirty)
            {
                this.IsDirty = undoManager.IsDirty;
            }
        }

        #endregion Другие обработчики событий

        #region Другие функции
        /// <summary>
        /// Создает контекстное меню
        /// </summary>
        void CreateContextMenu()
        {
            MenuItem menuItem;
            contextMenu = new ContextMenu();

            menuItem = new MenuItem();
            menuItem.Header = "Select All";
            menuItem.Tag = ContextMenuCommand.SelectAll;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Unselect All";
            menuItem.Tag = ContextMenuCommand.UnselectAll;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Delete";
            menuItem.Tag = ContextMenuCommand.Delete;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Delete All";
            menuItem.Tag = ContextMenuCommand.DeleteAll;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            contextMenu.Items.Add(new Separator());

            menuItem = new MenuItem();
            menuItem.Header = "Move to Front";
            menuItem.Tag = ContextMenuCommand.MoveToFront;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Move to Back";
            menuItem.Tag = ContextMenuCommand.MoveToBack;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            contextMenu.Items.Add(new Separator());

            menuItem = new MenuItem();
            menuItem.Header = "Undo";
            menuItem.Tag = ContextMenuCommand.Undo;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Redo";
            menuItem.Tag = ContextMenuCommand.Redo;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = "Set Properties";
            menuItem.Tag = ContextMenuCommand.SetProperties;
            menuItem.Click += new RoutedEventHandler(contextMenuItem_Click);
            contextMenu.Items.Add(menuItem);
        }

        /// <summary>
        /// Показывает контекстное меню
        /// </summary>
        void ShowContextMenu(MouseButtonEventArgs e)
        {
            Point point = e.GetPosition(this);
            GraphicsBase o = null;
            for (int i = graphicsList.Count - 1; i >= 0; i--)
            {
                if (((GraphicsBase)graphicsList[i]).MakeHitTest(point) == 0)
                {
                    o = (GraphicsBase)graphicsList[i];
                    break;
                }
            }
            if (o != null)
            {
                if (!o.IsSelected)
                {
                    UnselectAll();
                }
                o.IsSelected = true;
            }
            else
            {
                UnselectAll();
            }
            UpdateState();

            MenuItem item;
            /// Включает/выключает пункты меню
            foreach (object obj in contextMenu.Items)
            {
                item = obj as MenuItem;
                if (item != null)
                {
                    ContextMenuCommand command = (ContextMenuCommand)item.Tag;
                    switch (command)
                    {
                        case ContextMenuCommand.SelectAll:
                            item.IsEnabled = CanSelectAll;
                            break;
                        case ContextMenuCommand.UnselectAll:
                            item.IsEnabled = CanUnselectAll;
                            break;
                        case ContextMenuCommand.Delete:
                            item.IsEnabled = CanDelete;
                            break;
                        case ContextMenuCommand.DeleteAll:
                            item.IsEnabled = CanDeleteAll;
                            break;
                        case ContextMenuCommand.MoveToFront:
                            item.IsEnabled = CanMoveToFront;
                            break;
                        case ContextMenuCommand.MoveToBack:
                            item.IsEnabled = CanMoveToBack;
                            break;
                        case ContextMenuCommand.Undo:
                            item.IsEnabled = CanUndo;
                            break;
                        case ContextMenuCommand.Redo:
                            item.IsEnabled = CanRedo;
                            break;
                        case ContextMenuCommand.SetProperties:
                            item.IsEnabled = CanSetProperties;
                            break;
                    }
                }
            }
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Отмена текущей, выполненяемой операции.
        /// Вызывается, когда теряется захват мыши или Esc нажимается
        /// </summary>
        void CancelCurrentOperation()
        {
            if (Tool == ToolType.Pointer)
            {
                if (graphicsList.Count > 0)
                {
                    if (graphicsList[graphicsList.Count - 1] is GraphicsSelectionRectangle)
                    {
                        // Удаляем прямоугольник выбора, если он существует
                        graphicsList.RemoveAt(graphicsList.Count - 1);
                    }
                    else
                    {
                        // Добавление действия в историю
                        toolPointer.AddChangeToHistory(this);
                    }
                }
            }
            else if (Tool > ToolType.Pointer && Tool < ToolType.Max)
            {
                // Удалить последний рисуемый объект
                if (graphicsList.Count > 0)
                {
                    graphicsList.RemoveAt(graphicsList.Count - 1);
                }
            }
            Tool = ToolType.Pointer;
            this.ReleaseMouseCapture();
            this.Cursor = HelperFunctions.DefaultCursor;
        }

        /// <summary>
        /// Скрывает редактируемый textbox.
        /// Вызывается из TextTool, когда пользователь нажал Enter или Esc,
        /// или из этого класса, когда пользователь нажимает на холсте.
        /// </summary>
        internal void HideTextbox(GraphicsText graphicsText)
        {
            if (toolText.TextBox == null)
            {
                return;
            }
            graphicsText.IsSelected = true;   // восстанавливает выбор, который был снят
            if (toolText.TextBox.Text.Trim().Length == 0)
            {
                // textbox пустой
                if (!String.IsNullOrEmpty(toolText.OldText))
                {
                    undoManager.AddCommandToHistory(new CommandDelete(this));
                }
                graphicsList.Remove(graphicsText);
            }
            else
            {
                if (!String.IsNullOrEmpty(toolText.OldText))
                {
                    if (toolText.TextBox.Text.Trim() != toolText.OldText) //текст был изменен
                    {
                        CommandChangeState command = new CommandChangeState(this);
                        graphicsText.Text = toolText.TextBox.Text.Trim();
                        graphicsText.RefreshDrawing();
                        command.NewState(this);
                        undoManager.AddCommandToHistory(command);
                    }
                }
                else  // новый текст был добавлен
                {
                    graphicsText.Text = toolText.TextBox.Text.Trim();
                    graphicsText.RefreshDrawing();
                    undoManager.AddCommandToHistory(new CommandAdd(graphicsText));
                }
            }
            this.Children.Remove(toolText.TextBox);
            toolText.TextBox = null;

            // Это позволяет отключить все ApplicationCommands,
            // в то время, как текстовое поле активно
            this.Focus();
        }

        /// <summary>
        /// Открывает на месте нажатия окно редактирования
        /// </summary>
        void HandleDoubleClick(MouseButtonEventArgs e)
        {
            Point point = e.GetPosition(this);
            for (int i = graphicsList.Count - 1; i >= 0; i--)
            {
                GraphicsText t = graphicsList[i] as GraphicsText;
                if (t != null)
                {
                    if (t.Contains(point))
                    {
                        toolText.CreateTextBox(t, this);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Добавляет команду в историю
        /// </summary>
        internal void AddCommandToHistory(CommandBase command)
        {
            undoManager.AddCommandToHistory(command);
        }

        /// <summary>
        /// Очищает историю
        /// </summary>
        void ClearHistory()
        {
            undoManager.ClearHistory();
        }

        /// <summary>
        /// Обновляет состояние зависимых свойств
        /// используемые для редактирования команд.
        /// Вызывается при изменении состояния полотна
        /// </summary>
        void UpdateState()
        {
            bool hasObjects = (this.Count > 0);
            bool hasSelectedObjects = (this.SelectionCount > 0);

            CanSelectAll = hasObjects;
            CanUnselectAll = hasObjects;
            CanDelete = hasSelectedObjects;
            CanDeleteAll = hasObjects;
            CanMoveToFront = hasSelectedObjects;
            CanMoveToBack = hasSelectedObjects;
            CanSetProperties = HelperFunctions.CanApplyProperties(this);
        }

        #endregion Другие функции
    }
}
