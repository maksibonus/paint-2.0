﻿namespace DrawToolsLib
{
    /// <summary>
    /// Добавляет новый экземляр команды
    /// </summary>
    class CommandAdd : CommandBase
    {
        PropertiesGraphicsBase newObjectClone;

        // Создает команду с экземпляром DrawObject и добавляет в лист
        public CommandAdd(GraphicsBase newObject)
            : base()
        {
            this.newObjectClone = newObject.CreateSerializedObject();
        }

        /// <summary>
        /// Удаляет добавляемый объект
        /// </summary>
        public override void Undo(DrawingCanvas drawingCanvas)
        {
            GraphicsBase objectToDelete = null;

            // Удаляем объект из списка
            foreach (GraphicsBase b in drawingCanvas.GraphicsList)
            {
                if (b.Id == newObjectClone.ID)
                {
                    objectToDelete = b;
                    break;
                }
            }
            if (objectToDelete != null)
            {
                drawingCanvas.GraphicsList.Remove(objectToDelete);
            }
        }

        /// <summary>
        /// Добавляет объект снова
        /// </summary>
        public override void Redo(DrawingCanvas drawingCanvas)
        {
            HelperFunctions.UnselectAll(drawingCanvas);

            // Создаем полный объект клона и добавием его в список
            drawingCanvas.GraphicsList.Add(newObjectClone.CreateGraphics());
            drawingCanvas.RefreshClip();
        }
    }
}

