﻿using System.Collections.Generic;

namespace DrawToolsLib
{
    /// <summary>
    /// Команда удаления
    /// </summary>
    class CommandDelete : CommandBase
    {
        // Содержит выбранные объекты, которые будут удалены
        List<PropertiesGraphicsBase> cloneList;
        // Содержит индексы объектов, которые будут удалены
        List<int> indexes;

        public CommandDelete(DrawingCanvas drawingCanvas)
        {
            cloneList = new List<PropertiesGraphicsBase>();
            indexes = new List<int>();
            int currentIndex = 0;
            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                cloneList.Add(g.CreateSerializedObject());
                indexes.Add(currentIndex);
                currentIndex++;
            }
        }

        /// <summary>
        /// Восстанавливает удаленные объекты
        /// </summary>
        public override void Undo(DrawingCanvas drawingCanvas)
        {
            int currentIndex = 0;
            int indexToInsert;

            foreach (PropertiesGraphicsBase o in cloneList)
            {
                indexToInsert = indexes[currentIndex];
                if (indexToInsert >= 0 && indexToInsert <= drawingCanvas.GraphicsList.Count)
                {
                    drawingCanvas.GraphicsList.Insert(indexToInsert, o.CreateGraphics());
                }
                else
                {
                    drawingCanvas.GraphicsList.Add(o.CreateGraphics());
                    System.Diagnostics.Trace.WriteLine("CommandDelete.Undo - некорректный индекс!");
                }
                currentIndex++;
            }
            drawingCanvas.RefreshClip();
        }

        /// <summary>
        /// Удаляет объекты снова
        /// </summary>
        public override void Redo(DrawingCanvas drawingCanvas)
        {
            int n = drawingCanvas.GraphicsList.Count;

            for (int i = n - 1; i >= 0; i--)
            {
                bool toDelete = false;
                GraphicsBase currentObject = (GraphicsBase)drawingCanvas.GraphicsList[i];
                foreach (PropertiesGraphicsBase o in cloneList)
                {
                    if (o.ID == currentObject.Id)
                    {
                        toDelete = true;
                        break;
                    }
                }
                if (toDelete)
                {
                    drawingCanvas.GraphicsList.RemoveAt(i);
                }
            }
        }
    }
}
