﻿using System;
using System.Windows.Media;
using System.Xml.Serialization;

namespace DrawToolsLib
{
    /// <summary>
    /// Вспомогательный класс, используемый для сериализации XML.
    /// Содержит массив SerializedGraphicsBase экземпляров
    /// </summary>

    [XmlRoot("Graphics")]
    public class SerializationHelper
    {
        PropertiesGraphicsBase[] graphics;

        /// <summary>
        /// Необходимый констуктор по умолчанию
        /// </summary>
        public SerializationHelper()
        { }

        /// <summary>
        /// Этот конструктор используется для сериализации
        /// </summary>
        public SerializationHelper(VisualCollection collection)
        {
            int i = 0;

            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            graphics = new PropertiesGraphicsBase[collection.Count];
            foreach (GraphicsBase g in collection)
            {
                graphics[i++] = g.CreateSerializedObject();
            }
        }

        /// <summary>
        /// Когда класс сериализуется, массив graphics заполняется и сохраняется в файл формата XML.
        /// Когда класс десериализуется, массив graphics загружается из XML-файла и затем используется для создания VisualCollection
        /// </summary>
        [XmlArrayItem(typeof(PropertiesGraphicsEllipse)),
         XmlArrayItem(typeof(PropertiesGraphicsLine)),
         XmlArrayItem(typeof(PropertiesGraphicsPolyLine)),
         XmlArrayItem(typeof(PropertiesGraphicsRectangle)),
         XmlArrayItem(typeof(PropertiesGraphicsText))]
        public PropertiesGraphicsBase[] Graphics
        {
            get { return graphics; }
            set { graphics = value; }
        }
    }
}

