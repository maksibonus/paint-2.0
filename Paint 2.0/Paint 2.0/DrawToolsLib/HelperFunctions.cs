﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Вспомогательный класс, который содержит общие вспомогательные функции и свойства.
    /// </summary>
    static class HelperFunctions
    {
        /// <summary>
        /// Курсор
        /// </summary>
        public static Cursor DefaultCursor
        {
            get
            {
                return Cursors.Arrow;
            }
        }

        /// <summary>
        /// Выделяет все объекты
        /// </summary>
        public static void SelectAll(DrawingCanvas drawingCanvas)
        {
            for (int i = 0; i < drawingCanvas.Count; i++)
            {
                drawingCanvas[i].IsSelected = true;
            }
        }

        /// <summary>
        /// Отменяет выделение всех объектов
        /// </summary>
        public static void UnselectAll(DrawingCanvas drawingCanvas)
        {
            for (int i = 0; i < drawingCanvas.Count; i++)
            {
                drawingCanvas[i].IsSelected = false;
            }
        }

        /// <summary>
        /// Удаляет выбранные объекты
        /// </summary>
        public static void DeleteSelection(DrawingCanvas drawingCanvas)
        {
            CommandDelete command = new CommandDelete(drawingCanvas);
            bool wasChange = false;

            for (int i = drawingCanvas.Count - 1; i >= 0; i--)
            {
                if (drawingCanvas[i].IsSelected)
                {
                    drawingCanvas.GraphicsList.RemoveAt(i);
                    wasChange = true;
                }
            }

            if (wasChange)
            {
                drawingCanvas.AddCommandToHistory(command);
            }
        }

        /// <summary>
        /// Удаляет все объекты
        /// </summary>
        public static void DeleteAll(DrawingCanvas drawingCanvas)
        {
            if (drawingCanvas.GraphicsList.Count > 0)
            {
                drawingCanvas.AddCommandToHistory(new CommandDeleteAll(drawingCanvas));

                drawingCanvas.GraphicsList.Clear();
            }

        }

        /// <summary>
        /// Выносит выделенный объект на передний план
        /// </summary>
        public static void MoveSelectionToFront(DrawingCanvas drawingCanvas)
        {
            List<GraphicsBase> list = new List<GraphicsBase>();
            CommandChangeOrder command = new CommandChangeOrder(drawingCanvas);
            for (int i = drawingCanvas.Count - 1; i >= 0; i--)
            {
                if (drawingCanvas[i].IsSelected)
                {
                    list.Insert(0, drawingCanvas[i]);
                    drawingCanvas.GraphicsList.RemoveAt(i);
                }
            }

            // Добавляем все из временного листа в GraphicsList
            foreach (GraphicsBase g in list)
            {
                drawingCanvas.GraphicsList.Add(g);
            }
            if (list.Count > 0)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
        }

        /// <summary>
        /// Выносит выделенный объект на задний план
        /// </summary>
        public static void MoveSelectionToBack(DrawingCanvas drawingCanvas)
        {
            List<GraphicsBase> list = new List<GraphicsBase>();
            CommandChangeOrder command = new CommandChangeOrder(drawingCanvas);
            for (int i = drawingCanvas.Count - 1; i >= 0; i--)
            {
                if (drawingCanvas[i].IsSelected)
                {
                    list.Add(drawingCanvas[i]);
                    drawingCanvas.GraphicsList.RemoveAt(i);
                }
            }
            // Добавляем все из временного листа в начало GraphicsList
            foreach (GraphicsBase g in list)
            {
                drawingCanvas.GraphicsList.Insert(0, g);
            }

            if (list.Count > 0)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
        }

        /// <summary>
        /// Применяем новую ширину линии.
        /// LineWidth устанавливается для всех объектов, кроме GraphicsText
        /// </summary>
        public static bool ApplyLineWidth(DrawingCanvas drawingCanvas, double value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;
            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                if (g is GraphicsRectangle ||
                     g is GraphicsEllipse ||
                     g is GraphicsLine ||
                     g is GraphicsPolyLine)
                {
                    if (g.LineWidth != value)
                    {
                        g.LineWidth = value;
                        wasChange = true;
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем новый цвет
        /// </summary>
        public static bool ApplyColor(DrawingCanvas drawingCanvas, Color value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                if (g.ObjectColor != value)
                {
                    g.ObjectColor = value;
                    wasChange = true;
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем новое семество шрифтов
        /// </summary>
        public static bool ApplyFontFamily(DrawingCanvas drawingCanvas, string value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                GraphicsText gt = g as GraphicsText;

                if (gt != null)
                {
                    if (gt.TextFontFamilyName != value)
                    {
                        gt.TextFontFamilyName = value;
                        wasChange = true;
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем новый столь шрифта
        /// </summary>
        public static bool ApplyFontStyle(DrawingCanvas drawingCanvas, FontStyle value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.GraphicsList)
            {
                if (g.IsSelected)
                {
                    GraphicsText gt = g as GraphicsText;

                    if (gt != null)
                    {
                        if (gt.TextFontStyle != value)
                        {
                            gt.TextFontStyle = value;
                            wasChange = true;
                        }
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем насыщенность шрифта
        /// </summary>
        public static bool ApplyFontWeight(DrawingCanvas drawingCanvas, FontWeight value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                GraphicsText gt = g as GraphicsText;

                if (gt != null)
                {
                    if (gt.TextFontWeight != value)
                    {
                        gt.TextFontWeight = value;
                        wasChange = true;
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем новую ширину шрифта
        /// </summary>
        public static bool ApplyFontStretch(DrawingCanvas drawingCanvas, FontStretch value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                GraphicsText gt = g as GraphicsText;

                if (gt != null)
                {
                    if (gt.TextFontStretch != value)
                    {
                        gt.TextFontStretch = value;
                        wasChange = true;
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Применяем новый размер шрифта
        /// </summary>
        public static bool ApplyFontSize(DrawingCanvas drawingCanvas, double value, bool addToHistory)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            foreach (GraphicsBase g in drawingCanvas.Selection)
            {
                GraphicsText gt = g as GraphicsText;

                if (gt != null)
                {
                    if (gt.TextFontSize != value)
                    {
                        gt.TextFontSize = value;
                        wasChange = true;
                    }
                }
            }
            if (wasChange && addToHistory)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
            return wasChange;
        }

        /// <summary>
        /// Возвращает true, если в данный момент активные свойства (ширина линии, цвет и т.д.)
        /// могут быть применены к выбранным элементам.
        /// </summary>
        public static bool CanApplyProperties(DrawingCanvas drawingCanvas)
        {
            foreach (GraphicsBase graphicsBase in drawingCanvas.GraphicsList)
            {
                if (!graphicsBase.IsSelected)
                {
                    continue;
                }
                if (graphicsBase.ObjectColor != drawingCanvas.ObjectColor)
                {
                    return true;
                }
                GraphicsText graphicsText = graphicsBase as GraphicsText;
                if (graphicsText == null)
                {
                    if (graphicsBase.LineWidth != drawingCanvas.LineWidth)
                    {
                        return true;
                    }
                }
                else
                {
                    if (graphicsText.TextFontFamilyName != drawingCanvas.TextFontFamilyName)
                    {
                        return true;
                    }
                    if (graphicsText.TextFontSize != drawingCanvas.TextFontSize)
                    {
                        return true;
                    }
                    if (graphicsText.TextFontStretch != drawingCanvas.TextFontStretch)
                    {
                        return true;
                    }
                    if (graphicsText.TextFontStyle != drawingCanvas.TextFontStyle)
                    {
                        return true;
                    }
                    if (graphicsText.TextFontWeight != drawingCanvas.TextFontWeight)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Применяет текущие свойства к объекту
        /// </summary>
        public static void ApplyProperties(DrawingCanvas drawingCanvas)
        {
            CommandChangeState command = new CommandChangeState(drawingCanvas);
            bool wasChange = false;

            // Ширина линии
            if (ApplyLineWidth(drawingCanvas, drawingCanvas.LineWidth, false))
            {
                wasChange = true;
            }

            // Цвет
            if (ApplyColor(drawingCanvas, drawingCanvas.ObjectColor, false))
            {
                wasChange = true;
            }

            // Свойства шрифта
            if (ApplyFontFamily(drawingCanvas, drawingCanvas.TextFontFamilyName, false))
            {
                wasChange = true;
            }
            if (ApplyFontSize(drawingCanvas, drawingCanvas.TextFontSize, false))
            {
                wasChange = true;
            }
            if (ApplyFontStretch(drawingCanvas, drawingCanvas.TextFontStretch, false))
            {
                wasChange = true;
            }
            if (ApplyFontStyle(drawingCanvas, drawingCanvas.TextFontStyle, false))
            {
                wasChange = true;
            }
            if (ApplyFontWeight(drawingCanvas, drawingCanvas.TextFontWeight, false))
            {
                wasChange = true;
            }
            if (wasChange)
            {
                command.NewState(drawingCanvas);
                drawingCanvas.AddCommandToHistory(command);
            }
        }
    }
}

