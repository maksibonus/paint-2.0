﻿using System;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Свойства прямоугольника
    /// </summary>
    public class PropertiesGraphicsRectangle : PropertiesGraphicsBase
    {
        private double left;
        private double top;
        private double right;
        private double bottom;
        private double lineWidth;
        private Color objectColor;

        public PropertiesGraphicsRectangle()
        { }

        public PropertiesGraphicsRectangle(GraphicsRectangle rectangle)
        {
            if (rectangle == null)
            {
                throw new ArgumentNullException("rectangle");
            }
            left = rectangle.Left;
            top = rectangle.Top;
            right = rectangle.Right;
            bottom = rectangle.Bottom;
            lineWidth = rectangle.LineWidth;
            objectColor = rectangle.ObjectColor;
            actualScale = rectangle.ActualScale;
            ID = rectangle.Id;
            selected = rectangle.IsSelected;
        }

        public override GraphicsBase CreateGraphics()
        {
            GraphicsBase b = new GraphicsRectangle(left, top, right, bottom, lineWidth, objectColor, actualScale);

            // Устанавливаем ID прямоугольника, который был нарисован в текущем сеансе
            if (this.ID != 0)
            {
                b.Id = this.ID;
                b.IsSelected = this.selected;
            }
            return b;
        }

        #region Свойства
        /// <summary>
        /// Левая сторона
        /// </summary>
        public double Left
        {
            get { return left; }
            set { left = value; }
        }

        /// <summary>
        /// Верхняя сторона
        /// </summary>
        public double Top
        {
            get { return top; }
            set { top = value; }
        }

        /// <summary>
        /// Правая сторона
        /// </summary>
        public double Right
        {
            get { return right; }
            set { right = value; }
        }

        /// <summary>
        /// Нижняя сторона
        /// </summary>
        public double Bottom
        {
            get { return bottom; }
            set { bottom = value; }
        }

        /// <summary>
        /// Ширина линии
        /// </summary>
        public double LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        /// <summary>
        /// Цвет
        /// </summary>
        public Color ObjectColor
        {
            get { return objectColor; }
            set { objectColor = value; }
        }

        #endregion Свойства
    }
}

