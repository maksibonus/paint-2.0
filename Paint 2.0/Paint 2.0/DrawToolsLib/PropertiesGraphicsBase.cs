﻿using System.Xml.Serialization;

namespace DrawToolsLib
{
    /// <summary>
    /// Базовый класс для сериализации вспомогательных классов
    /// </summary>
    public abstract class PropertiesGraphicsBase
    {
        [XmlIgnore]
        internal int ID;

        [XmlIgnore]
        internal bool selected;

        [XmlIgnore]
        internal double actualScale;

        public abstract GraphicsBase CreateGraphics();
    }
}
