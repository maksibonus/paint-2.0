﻿using System.IO;
using System.Windows;
using System.Windows.Input;

namespace DrawToolsLib
{
    /// <summary>
    /// Инструмент ломанной линии
    /// </summary>
    class ToolPolyLine : ToolObject
    {
        private double lastX;
        private double lastY;
        private GraphicsPolyLine newPolyLine;
        private const double minDistance = 15;

        public ToolPolyLine()
        {
            MemoryStream stream = new MemoryStream(Properties.Resources.Pencil);
            ToolCursor = new Cursor(stream);
        }

        /// <summary>
        /// Созадет новый объект
        /// </summary>
        public override void OnMouseDown(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(drawingCanvas);
            newPolyLine = new GraphicsPolyLine(
                new Point[]
                {
                    p,
                    new Point(p.X + 1, p.Y + 1)
                },
                drawingCanvas.LineWidth,
                drawingCanvas.ObjectColor,
                drawingCanvas.ActualScale);

            AddNewObject(drawingCanvas, newPolyLine);
            lastX = p.X;
            lastY = p.Y;
        }

        /// <summary>
        /// Устанавливает курсор и изменяет размеры
        /// </summary>
        public override void OnMouseMove(DrawingCanvas drawingCanvas, MouseEventArgs e)
        {
            drawingCanvas.Cursor = ToolCursor;
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }
            if (!drawingCanvas.IsMouseCaptured)
            {
                return;
            }
            if (newPolyLine == null)
            {
                return;
            }
            Point p = e.GetPosition(drawingCanvas);
            double distance = (p.X - lastX) * (p.X - lastX) + (p.Y - lastY) * (p.Y - lastY);
            double d = drawingCanvas.ActualScale <= 0 ?
                minDistance * minDistance :
                minDistance * minDistance / drawingCanvas.ActualScale;
            if (distance < d)
            {
                newPolyLine.MoveHandleTo(p, newPolyLine.HandleCount);
            }
            else
            {
                newPolyLine.AddPoint(p);
                lastX = p.X;
                lastY = p.Y;
            }
        }

        public override void OnMouseUp(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            newPolyLine = null;
            base.OnMouseUp(drawingCanvas, e);
        }
    }
}

