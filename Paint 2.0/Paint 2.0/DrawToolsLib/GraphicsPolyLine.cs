﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Графический объект ломанной линии
    /// </summary>
    public class GraphicsPolyLine : GraphicsBase
    {
        #region Члены
        // Содержит фигуры
        protected PathGeometry pathGeometry;

        // Точки для геометрии
        protected Point[] points;

        // Обработчик курсора
        static Cursor handleCursor;

        #endregion Члены

        #region Конструкторы
        public GraphicsPolyLine(Point[] points, double lineWidth, Color objectColor, double actualScale)
        {
            Fill(points, lineWidth, objectColor, actualScale);
        }

        public GraphicsPolyLine()
            : this(new Point[] { new Point(0.0, 0.0), new Point(100.0, 100.0) }, 1.0, Colors.Black, 1.0)
        { }

        static GraphicsPolyLine()
        {
            MemoryStream stream = new MemoryStream(Properties.Resources.PolyHandle);
            handleCursor = new Cursor(stream);
        }

        #endregion Конструкторы

        #region Другие функции
        /// <summary>
        /// Конвертирует сегменты в массив точек
        /// </summary>
        void MakePoints()
        {
            points = new Point[pathGeometry.Figures[0].Segments.Count + 1];
            points[0] = pathGeometry.Figures[0].StartPoint;
            for (int i = 0; i < pathGeometry.Figures[0].Segments.Count; i++)
            {
                points[i + 1] = ((LineSegment)(pathGeometry.Figures[0].Segments[i])).Point;
            }
        }

        /// <summary>
        /// Возвращает массив точек
        /// </summary>
        public Point[] GetPoints()
        {
            return points;
        }

        /// <summary>
        /// Конвертирует массив точек в геометрическую фигуру
        /// </summary>
        void MakeGeometryFromPoints(ref Point[] points)
        {
            if (points == null)
            {
                points = new Point[2];
            }

            PathFigure figure = new PathFigure();

            if (points.Length >= 1)
            {
                figure.StartPoint = points[0];
            }

            for (int i = 1; i < points.Length; i++)
            {
                LineSegment segment = new LineSegment(points[i], true);
                segment.IsSmoothJoin = true;
                figure.Segments.Add(segment);
            }
            pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(figure);
            MakePoints();
        }

        // Для конструктора
        void Fill(Point[] points, double lineWidth, Color objectColor, double actualScale)
        {
            MakeGeometryFromPoints(ref points);
            this.graphicsLineWidth = lineWidth;
            this.graphicsObjectColor = objectColor;
            this.graphicsActualScale = actualScale;
        }


        /// <summary>
        /// Добавить новую точку
        /// </summary>
        public void AddPoint(Point point)
        {
            LineSegment segment = new LineSegment(point, true);
            segment.IsSmoothJoin = true;
            pathGeometry.Figures[0].Segments.Add(segment);
            MakePoints();
        }

        #endregion Другие функции

        #region Перегрузки
        public override void Draw(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException("drawingContext");
            }
            drawingContext.DrawGeometry(
                null,
                new Pen(new SolidColorBrush(ObjectColor), ActualLineWidth),
                pathGeometry);
            base.Draw(drawingContext);
        }

        public override bool Contains(Point point)
        {
            return pathGeometry.FillContains(point) ||
                pathGeometry.StrokeContains(new Pen(Brushes.Black, LineHitTestWidth), point);
        }

        public override PropertiesGraphicsBase CreateSerializedObject()
        {
            return new PropertiesGraphicsPolyLine(this);
        }

        public override int HandleCount
        {
            get
            {
                return pathGeometry.Figures[0].Segments.Count + 1;
            }
        }

        public override Point GetHandle(int handleNumber)
        {
            if (handleNumber < 1)
                handleNumber = 1;
            if (handleNumber > points.Length)
                handleNumber = points.Length;
            return points[handleNumber - 1];
        }

        public override Cursor GetHandleCursor(int handleNumber)
        {
            return handleCursor;
        }

        public override void MoveHandleTo(Point point, int handleNumber)
        {
            if (handleNumber == 1)
            {
                pathGeometry.Figures[0].StartPoint = point;
            }
            else
            {
                ((LineSegment)(pathGeometry.Figures[0].Segments[handleNumber - 2])).Point = point;
            }
            MakePoints();
            RefreshDrawing();
        }

        public override void Move(double deltaX, double deltaY)
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i].X += deltaX;
                points[i].Y += deltaY;
            }

            MakeGeometryFromPoints(ref points);
            RefreshDrawing();
        }

        public override int MakeHitTest(Point point)
        {
            if (IsSelected)
            {
                for (int i = 1; i <= HandleCount; i++)
                {
                    if (GetHandleRectangle(i).Contains(point))
                        return i;
                }
            }
            if (Contains(point))
                return 0;
            return -1;
        }

        public override bool IntersectsWith(Rect rectangle)
        {
            RectangleGeometry rg = new RectangleGeometry(rectangle);
            PathGeometry p = Geometry.Combine(rg, pathGeometry, GeometryCombineMode.Intersect, null);
            return (!p.IsEmpty());
        }

        #endregion Переопределения
    }
}

