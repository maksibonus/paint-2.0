﻿using System.Collections.Generic;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Изменяет состояния существующих объектов:
    /// перемещение, изменение размера, изменение свойств и т.д.
    /// </summary>
    class CommandChangeState : CommandBase
    {
        // Выбранные объекты до операции
        List<PropertiesGraphicsBase> listBefore;

        // Выбранные объекты после операции
        List<PropertiesGraphicsBase> listAfter;

        public CommandChangeState(DrawingCanvas drawingCanvas)
        {
            FillList(drawingCanvas.GraphicsList, ref listBefore);
        }

        /// <summary>
        // Функция, которая вызывается после операции
        /// <summary>
        public void NewState(DrawingCanvas drawingCanvas)
        {
            FillList(drawingCanvas.GraphicsList, ref listAfter);
        }

        /// <summary>
        /// Восстановить выбор состояния до изменения
        /// </summary>
        public override void Undo(DrawingCanvas drawingCanvas)
        {
            ReplaceObjects(drawingCanvas.GraphicsList, listBefore);
            drawingCanvas.RefreshClip();
        }

        /// <summary>
        /// Восстановить выбор состояния после изменения
        /// </summary>
        public override void Redo(DrawingCanvas drawingCanvas)
        {
            ReplaceObjects(drawingCanvas.GraphicsList, listAfter);
            drawingCanvas.RefreshClip();
        }

        /// <summary>
        // Заменяет объекты в graphicsList объектами из клона листа
        /// </summary>
        private static void ReplaceObjects(VisualCollection graphicsList, List<PropertiesGraphicsBase> list)
        {
            for (int i = 0; i < graphicsList.Count; i++)
            {
                PropertiesGraphicsBase replacement = null;
                foreach (PropertiesGraphicsBase o in list)
                {
                    if (o.ID == ((GraphicsBase)graphicsList[i]).Id)
                    {
                        replacement = o;
                        break;
                    }
                }
                if (replacement != null)
                {
                    graphicsList.RemoveAt(i);
                    graphicsList.Insert(i, replacement.CreateGraphics());
                }
            }
        }

        /// <summary>
        /// Заполняет список выбранными объектами
        /// </summary>
        private static void FillList(VisualCollection graphicsList, ref List<PropertiesGraphicsBase> listToFill)
        {
            listToFill = new List<PropertiesGraphicsBase>();
            foreach (GraphicsBase g in graphicsList)
            {
                if (g.IsSelected)
                {
                    listToFill.Add(g.CreateSerializedObject());
                }
            }
        }
    }
}
