﻿namespace DrawToolsLib
{
    /// <summary>
    /// Инструменты рисования
    /// </summary>
    public enum ToolType
    {
        None,
        Pointer,
        Rectangle,
        Ellipse,
        Line,
        PolyLine,
        Text,
        Max
    };

    /// <summary>
    /// Команды контекстного меню
    /// </summary>
    internal enum ContextMenuCommand
    {
        SelectAll,
        UnselectAll,
        Delete,
        DeleteAll,
        MoveToFront,
        MoveToBack,
        Undo,
        Redo,
        SetProperties
    };
}

