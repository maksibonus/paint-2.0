﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Media;

namespace DrawToolsLib
{
    /// <summary>
    /// Изменения порядка объектов
    /// </summary>
    class CommandChangeOrder : CommandBase
    {
        // Лист до операции
        List<int> listBefore;

        // Лист после операции
        List<int> listAfter;

        public CommandChangeOrder(DrawingCanvas drawingCanvas)
        {
            FillList(drawingCanvas.GraphicsList, ref listBefore);
        }

        /// <summary>
        /// Функция, которая вызывается после операции
        /// </summary>
        public void NewState(DrawingCanvas drawingCanvas)
        {
            FillList(drawingCanvas.GraphicsList, ref listAfter);
        }

        /// <summary>
        /// Восстанавливаем порядок до изменения
        /// </summary>
        public override void Undo(DrawingCanvas drawingCanvas)
        {
            ChangeOrder(drawingCanvas.GraphicsList, listBefore);
        }

        /// <summary>
        /// Восстанавливаем порядок после изменения
        /// </summary>
        public override void Redo(DrawingCanvas drawingCanvas)
        {
            ChangeOrder(drawingCanvas.GraphicsList, listAfter);
        }

        /// <summary>
        // Лист ID объектов
        /// </summary>
        private static void FillList(VisualCollection graphicsList, ref List<int> listToFill)
        {
            listToFill = new List<int>();

            foreach (GraphicsBase g in graphicsList)
            {
                listToFill.Add(g.Id);
            }
        }

        /// <summary>
        // Устанавливает порядок отбора в graphicsList по списку ID
        /// </summary>
        private static void ChangeOrder(VisualCollection graphicsList, List<int> indexList)
        {
            List<GraphicsBase> tmpList = new List<GraphicsBase>();
            foreach (int id in indexList)
            {
                GraphicsBase objectToMove = null;

                foreach (GraphicsBase g in graphicsList)
                {
                    if (g.Id == id)
                    {
                        objectToMove = g;
                        break;
                    }
                }

                if (objectToMove != null)
                {
                    tmpList.Add(objectToMove);
                    graphicsList.Remove(objectToMove);
                }
            }

            // tmpList содержит объекты в правильном порядке
            foreach (GraphicsBase g in tmpList)
            {
                graphicsList.Add(g);
            }
        }
    }
}

